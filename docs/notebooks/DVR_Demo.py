# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <headingcell level=1>

# Initialization

# <markdowncell>

# To run this iPython notebook as a notebook, you will need a fairly recent version of iPython (tested with version 0.13.1).  You can start the notebook server by running the following command in the same directory as the downloaded ``.ipynb`` file:
# 
#     ipython notebook --pylab-inline
# 
# This should start the server, and open a web-browser showing you all the ``.ipynb`` files in the directory.  (The ``--pylab=inline`` option will generate the plots in the notebook rather than spawning a separate window.)
# 
# You should also be able to execute the code by copying and pasting into a regular python session (in which case, you should *not* copy the first block which does iPython specific initialization.)  To view and interact with the plots, however, you will need to start an appropriate interpreter.  The following should work for iPython:
# 
#     ipython --pylab
# 
# Note that some of the longer blocks will probably not paste very well due to blank lines etc.  You can instead use the ipython ``%paste`` magic function or ``%cpaste`` to include these.

# <codecell>

# This enables pretty printing in ipython but may fail with older versions and will not work on the command line.
from __future__ import division
try:
    import sympy.interactive.ipythonprinting
    sympy.interactive.ipythonprinting.init_printing()
except:
    pass

# <codecell>

# These are standard imports for pylab but included here for people running plain python or ipython
import numpy as np
import scipy as sp
from matplotlib import pyplot as plt

# <headingcell level=1>

# Scarf II Potential with three bound states

# <markdowncell>

# Here we analyze the Scarf II potential which has an exact spectrum (it is one of the potentials that can be analyzed using supersymmetric quantum mechanics):

# <codecell>

import sympy
from sympy import S
V_scarf = S('(A**2 + (-A**2 - A + B**2 + B*(2*A + 1)*sinh(x/a))/cosh(x/a)**2)/(2*a**2*m) - A**2/2/m')
E_scarf = S('(A**2 - (A - n)**2)/(2*a**2*m) - A**2/2/m')
V_scarf, E_scarf

# <markdowncell>

# We shall use parameters $A=3$, $B=1$, and $a=m=1$.  The wavefunctions also have an exact representation in terms of the Romanovski polynomials, but for simplicity we just record the three bound states for our choice of parameters.

# <codecell>

args = dict(A=3, B=1, a=1, m=1)
V = sympy.lambdify(['x'], V_scarf.subs(args), 'numpy')
En = sympy.lambdify(['n'], E_scarf.subs(args), 'numpy')
psi_scarf = [
    S('exp(-B*atan(sinh(x/a)))/(sqrt(a)*(cosh(x/a)**2)**(3/2))'),
    S('-(2*B + 5*sinh(x/a))*exp(-B*atan(sinh(x/a)))/(sqrt(a)*(cosh(x/a)**2)**(3/2))'),
    S('(4*B**2 + 16*B*sinh(x/a) + 12*sinh(x/a)**2 - 3)*exp(-B*atan(sinh(x/a)))/(sqrt(a)*(cosh(x/a)**2)**(3/2))')]
psi = [sympy.lambdify(['x'], _psi.subs(args), 'numpy') for _psi in psi_scarf]
psi_scarf

# <markdowncell>

# Here is the potential, the wavefunction of the first three bound states, and their energies:

# <codecell>

x = np.linspace(-6,6,100)
plt.plot(x, V(x), '-')
[(plt.plot(x, psi[_i](x), ls=_ls),
  plt.axhline(En(_i), ls=_ls, c='y')) for _i, _ls in enumerate(['--', '-.', ':'])]

# <markdowncell>

# Now we construct (without proof) the DVR basis sets both on a periodic lattice, and in infinite space.

# <codecell>

class DVR1D(object):
    r"""Sinc function basis for non-periodic functions over an interval
    `x0 +- L/2` with `N` points."""
    def __init__(self, N, L, x0=0.0):
        L = float(L)
        self.N = N
        self.L = L
        self.x0 = x0
        self.a = L/N
        self.n = np.arange(N)
        self.x = self.x0 + self.n*self.a - self.L/2.0 + self.a/2.0
        self.k_max = np.pi/self.a

    def H(self, V):
        """Return the Hamiltonian with the give potential."""
        _m = self.n[:, None]
        _n = self.n[None, :]
        K = 2.0*(-1)**(_m-_n)/(_m-_n)**2/self.a**2
        K[self.n, self.n] = np.pi**2/3/self.a**2
        K *= 0.5   # p^2/2/m
        V = np.diag(V(self.x))
        return K + V

    def F(self, x=None):
        """Return the DVR basis vectors"""
        if x is None:
            x_m = self.x[:, None]
        else:
            x_m = np.asarray(x)[:, None]
        x_n = self.x[None, :]
        return np.sinc((x_m-x_n)/self.a)/np.sqrt(self.a)


class DVRPeriodic(DVR1D):
    r"""Sinc function basis for periodic functions over an interval
    `x0 +- L/2` with `N` points."""
    def __init__(self, *v, **kw):
        # Small shift here for consistent abscissa
        DVR1D.__init__(self, *v, **kw)
        self.x -= self.a/2.0
        
    def H(self, V):
        """Return the Hamiltonian with the give potential."""
        _m = self.n[:, None]
        _n = self.n[None, :]
        _arg = np.pi*(_m-_n)/self.N
        if (0 == self.N % 2):
            K = 2.0*(-1)**(_m-_n)/np.sin(_arg)**2
            K[self.n, self.n] = (self.N**2 + 2.0)/3.0
        else:
            K = 2.0*(-1)**(_m-_n)*np.cos(_arg)/np.sin(_arg)**2
            K[self.n, self.n] = (self.N**2 - 1.0)/3.0
        K *= 0.5*(np.pi/self.L)**2   # p^2/2/m
        V = np.diag(V(self.x))
        return K + V

    def F(self, x=None):
        """Return the DVR basis vectors"""
        if x is None:
            x_m = self.x[:, None]
        else:
            x_m = np.asarray(x)[:, None]
        x_n = self.x[None, :]
        F = np.sinc((x_m-x_n)/self.a)/np.sinc((x_m-x_n)/self.L)/np.sqrt(self.a)
        if (0 == self.N % 2):
            F *= np.exp(-1j*np.pi*(x_m-x_n)/self.L)
        return F

# <markdowncell>

# We now demonstrate the convergence properties of these bases:

# <codecell>

def get_E(N=100, L=20.0, DVR=DVR1D):
    d = DVR(N=N, L=L)
    E, v = np.linalg.eigh(d.H(V=V))
    A = args['A']
    return E[:A], v[:,:A], d

# <codecell>

ks = [5.0, 10.0, 15.0, 20.0]
ls = [':', '-.', '--', '-']
cs = ['k', 'b', 'g']
L_max = 35.0
fig = plt.figure(figsize=(10,5))
ax = []
for _n, DVR in enumerate([DVR1D, DVRPeriodic]):
    ax.append(plt.subplot(1,2,_n+1))
    for k_max, _ls in zip(ks, ls):
        err = []
        Ls = []
        N_max = int(np.ceil(L_max/np.pi*k_max))
        Ns = np.arange(4,N_max,4)
        for _N in Ns:
            _L = np.pi*_N/k_max
            E, v, d = get_E(N=_N, L=_L, DVR=DVR)
            err.append(abs(E-En(np.arange(3))))
            Ls.append(_L)
        [plt.semilogy(Ls, _e, ls=_ls, c=cs[_i]) for _i, _e in enumerate(np.asarray(err).T)]
        if _n == 0:
            print k_max, N_max
    plt.title(DVR.__name__)
    plt.xlabel("L")
    if _n == 0:
        plt.ylabel("error (E)")

# <markdowncell>

# Now we take the UV converged solutions ($k_{\text{max}} = 20$) and plot the IR errors again along with the asymptotic form $E_L = E_{\infty} + a_0 e^{-k_\infty L}$:

# <codecell>

cs = ['k', 'b', 'g']
L_max = 35.0
k_max = 20.0
fig = plt.figure(figsize=(10,5))
ax = []
a0 = [100,100,100]
errs = []
for _n, DVR in enumerate([DVR1D, DVRPeriodic]):
    ax.append(plt.subplot(1,2,_n+1))
    err = []
    Es = []
    Ls = []
    N_max = int(np.ceil(L_max/np.pi*k_max))
    Ns = np.arange(4,N_max,4)
    for _N in Ns:
        _L = np.pi*_N/k_max
        E, v, d = get_E(N=_N, L=_L, DVR=DVR)
        Es.append(E)
        err.append(E-En(np.arange(3)))
        Ls.append(_L)
    errs.append(err)
    [plt.semilogy(Ls, abs(_e), ls=':', c=cs[_i]) for _i, _e in enumerate(np.asarray(err).T)]
    ind = np.where(Ls > 5)[0][0]
    def f_fit(a, n, i0, i1, _Ls=np.asarray(Ls), _Es=np.asarray(Es)):
        E_inf, a0, k_inf = a
        _E = E_inf + a0*np.exp(-2*_Ls[i0:i1]*k_inf)
        return np.log(abs(_Es[i0:i1, n] - E_inf)/a0) + 2*_Ls[i0:i1]*k_inf 
        return _Es[i0:i1,n] - _E
    #sp.optimize.leastsq(f_fit, [0, 1, 0])
    [plt.semilogy(Ls, a0[_i]*np.exp(-np.sqrt(-2.0*En(_i))*np.asarray(Ls)),
              ls='--', c=cs[_i]) for _i in xrange(3)]
    plt.title(DVR.__name__)
    plt.xlabel("L")
    if _n == 0:
        plt.ylabel("error (E)")
    plt.axis([0,35,1e-15,10])

# <markdowncell>

# Here is the conjugate figure: we now hold $L$ fixed

# <codecell>

k_max = 20.0
Ls = [5.0, 15.0, 25.0, 35.0]
ls = [':', '-.', '--', '-']
ls = [':', '-.', '--', '-']
cs = ['k', 'b', 'g']
fig = plt.figure(figsize=(10,5))
ax = []
for _n, DVR in enumerate([DVR1D, DVRPeriodic]):
    ax.append(plt.subplot(1,2,_n+1))
    for _L, _ls in zip(Ls, ls):
        err = []
        ks = []
        N_max = int(np.ceil(k_max*_L/np.pi))
        Ns = np.arange(4, N_max, 2)
        for _N in Ns:
            k_max = np.pi/_L*_N
            ks.append(k_max)
            E, v, d = get_E(N=_N, L=_L, DVR=DVR)
            err.append(abs(E-En(np.arange(3))))
        [plt.semilogy(ks, _e, ls=_ls, c=cs[_i]) 
         for _i, _e in enumerate(np.asarray(err).T)]
    plt.title(DVR.__name__)
    plt.xlabel("k_max")
    if _n == 0:
        plt.ylabel("error (E)")

# <markdowncell>

# It is not obvious from these plots, but the errors in the DVR1D class are positive as these correspond to Dirichlet-like boundary condition (all lattice points outside of the domain are forced to zero) while the DVRPeriodic errors alternate -- negative for the ground state, positive for the first excited state, and negative for the second excited state.  This is consistent to the splitting of the band structure due to the periodic images.

# <codecell>

plt.plot(np.asarray(errs)[0], '-')
plt.plot(np.asarray(errs)[1], '--')
plt.axis([0,35,-0.0001,0.0001])

# <headingcell level=1>

# HO potential

# <markdowncell>

# Now we compare the DVR basis with the HO basis by computing the HO spectrum using the DVR basis.  We start with fixed lattice constant $a=1 = \pi/k_c$ and vary the basis size $N$ adjusting $\omega = 2\pi/L$ where $L=\pi N/k_c$.  We then demonstrate the UV convergence by holding $L$ fixed and varying $N$ and $k_c$.

# <codecell>

Ns = [30, 40, 50]

def V(x):
   r"""HO potential"""
   return w**2*x**2/2.0

fig = plt.figure(figsize=(10,5))
ax = []
for _n, DVR in enumerate([DVR1D, DVRPeriodic]):
    ax.append(plt.subplot(1,2,_n+1))
    a = 1.0
    k_c = np.pi/a
    for _N in Ns:
        _L = np.pi*_N/k_c
        w = 2.0*np.pi/_L
        dvr = DVR(N=_N, L=_L)
        E = np.linalg.eigvalsh(dvr.H(V=V))
        n = np.arange(_N)
        _En = (n + 0.5)*w
        plt.semilogy(n, abs(E-_En) + 1e-16, 'b-+')

    _L = 30.0
    w = 2.0*np.pi/_L
    for _N in [60, 90]:
        a = _L / _N
        k_c = np.pi/a
        dvr = DVR(N=_N, L=_L)

        E = np.linalg.eigvalsh(dvr.H(V=V))
        n = np.arange(_N)
        _En = (n + 0.5)*w
        plt.semilogy(n, abs(E-_En) + 1e-16, 'r:.')

    plt.axis([0, 50, 1e-16, 100])
    plt.xlabel(r'$n$')
    if _n == 0:
        plt.ylabel(r'$\Delta E$')
    plt.title(DVR.__name__)

# <codecell>

# Show the plots if running as a script
if __name__ == '__main__':
    plt.show()

