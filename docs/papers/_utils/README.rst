.. -*- rst -*- -*- restructuredtext -*-

.. This file should be written using the restructure text
.. conventions.  It will be displayed on the bitbucket source page and
.. serves as the documentation of the directory.

LaTeX Styles for Paper
======================

This project contains a set of style files that I use for writing papers.  To
use these, just use the appropriate style in your paper.

Usage
=====
The skeleton of a paper is::

   \documentclass[10pt, aps, pra, nofootinbib, 
                  amsmath, amssymb, twocolumn,
                  preprintnumbers, showpacs,
                  raggedbottom,
                  floatfix]{revtex4-1}

   % My customized styling
   \newcommand{\figdir}{figures}     % Figs. in `figures/aps` or `figures/arXiv`
   \usepackage[arXiv,todo]{my_paper} % Use this for the arXiv
   %\usepackage[aps]{my_paper}       % Use this for APS

   \usepackage{my_acronyms}          % Acronyms for this paper

   \usepackage[utf8]{inputenc}       % So you can use unicode

   \begin{document}

   \title{A Great Paper}

   \author{Michael McNeil Forbes}

   \affiliation{Institute for Nuclear Theory, University of Washington,
     Seattle, Washington 98195--1550 USA}

   \begin{abstract}\noindent
     We do great things here.
   \end{abstract}

   \maketitle

   ...

   \bibliographystyle{apsrev4-1}
   \bibliography{local,master}    % I use a local.bib, and a global master.bib
   \end{document}

If you use ``latexmk``, then the included configuration will use ``pdflatex``
with ``synctex`` and put the output into ``_build`` to keep your directory
clean.

You can look at ``my_acronyms.sty`` as an example of how to use the glossaries
package (but for the final version, I would include these directly in the
paper).

Options
=======
Here are a couple of options supported by the ``my_paper.sty`` style:

``aps/arXiv``:
   Overall styling.  I think the ``arXiv`` version looks better, but it does not
   conform to APS standards (APS does not like smallcaps for example.)
``euler/noeuler``:
   If using ``arXiv``, then you can use Palatino/Euler fonts for the text and
   math (otherwise, use whatever your class defines -- Times for revtex).
``todo``:
   Enable margin to do notes with the ``\todo{}`` command.  These will not
   interrupt the flow of your text, but may cause some build problems if you
   have lots of floats.  This is disabled for the ``aps`` style.


.. Links
