\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{my_paper}[2013/05/05 Personal Style for arXiv and aps papers]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Options
%% 
%% aps: Format for APS journals (no smallcaps except programs etc., use times
%%      font etc.)
%% euler/noeuler: Use (don't use) the Euler fonts for math.
%% todo: Enable todo notes and \exclude
%%
%% One can also specify \figdir as the root of the figures directory

\newif\if@aps \@apsfalse
\newif\if@euler \@eulertrue
\newif\if@todo \@todofalse
\newif\if@midcaps \@midcapsfalse

\DeclareOption{aps}{\@apstrue}
\DeclareOption{arXiv}{\@apsfalse\@midcapstrue}
\DeclareOption{euler}{\@eulertrue}
\DeclareOption{noeuler}{\@eulerfalse}
\DeclareOption{todo}{\@todotrue}
\DeclareOption{midcaps}{\@midcapstrue}

\ProcessOptions\relax

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% General packages to help out

\RequirePackage[T1]{fontenc}
\RequirePackage{textcomp}

\RequirePackage{etoolbox}
\RequirePackage{etextools}
\RequirePackage[acronym]{glossaries}

\glsdisablehyper
\makeglossaries

% Allows unicode input of things like ö in Schrödinger.
\RequirePackage[utf8]{inputenc}

\RequirePackage[breaklinks]{hyperref}
\RequirePackage{graphicx}
\providecommand{\figdir}{./figures/}
\if@aps
  \edef\@graphicspath{{\figdir/},{\figdir/aps/}}
\else
  \edef\@graphicspath{{\figdir/},{\figdir/arXiv/}}
\fi
\expandnext{\graphicspath}{\@graphicspath}

\RequirePackage{dcolumn}
\RequirePackage{calc}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Typesetting

\RequirePackage[tracking]{microtype}
\SetTracking{ encoding = *, shape = sc }{ 25 }
\RequirePackage{xcolor}

% Bold math.  Make sure this comes *after* choosing the math font.
\AtBeginDocument{\RequirePackage{bm}}

% Nice text-fractions
\RequirePackage[nice]{nicefrac}

% Booktabs make niced table rules and spacing, but do not work with revtex.
%\RequirePackage{booktabs}

% Pdf notes that do not affect the typesetting.
\if@todo
  \RequirePackage{todonotes}
  \RequirePackage[color=orange, icon=Help]{pdfcomment}
  \renewcommand{\todo}[1]{%
    \ifinfloat{{\@todo[disable]{#1}\@todonotes@addElementToListOfTodos}%
               \pdfcomment[hoffset=-5pt]{#1}}
              {{\@todo[caption={#1}]{\pdfcomment[hoffset=-5pt]{#1}}}}}
  \providecommand\exclude[1]{}
\fi


% This package is great for formatting numbers with errors and units.  I use it
% in table I.  (Table II is so tight that I format it by hand and use the
% Palatino text fonts which are smaller and fit in the column.  Kind of a hack,
% I know.)
\RequirePackage{siunitx}
%\sisetup{mode=math,math-rm=\usefont{U}{zeur}{m}{n}{}\selectfont}
%\sisetup{math-rm=\mathnormal} % Needed so that Euler is used in equations.

% For chemical symbols
\RequirePackage[version=3]{mhchem}

\if@aps
  % Kill lettrine
  \providecommand{\lettrine}[2]{\noindent #1#2}
\else
  \RequirePackage{lettrine}
  % For some reason, one can only use \LettrineWidth if a config file is
  % specified.  I want the commands here though.
  \renewcommand{\DefaultOptionsFile}{NoFileOnPurposeHack.cfg}

  \if@euler
    % Palatino
    \usepackage[sc,osf]{mathpazo}
    \linespread{1.025}              % Palatino leads a little more leadding

    % Euler for math and numbers
    \RequirePackage[euler-digits,small]{eulervm}
    \AtBeginDocument{\renewcommand{\hbar}{\hslash}}

    % The Euler letter T looks nice in the first line.  Here are some tweaks to
    % make it look good.  This should hang out in the margin a bit.
    \renewcommand{\LettrineFontHook}{\usefont{U}{zeur}{m}{n}{}}
    \renewcommand{\LettrineTextFont}{\scshape}
    \LettrineOptionsFor{T}{
      lines=3,
      loversize=0.1,
      %lraise=-0.07,
      lraise=-0.03,
      lhang=0.49,
      findent=0.4em,
      nindent=-0.0\LettrineWidth-0.5em}

    % This is for the Palatino U
    \LettrineOptionsFor{U}{
      lines=3,
      loversize=0.05,
      %lraise=-0.07,
      lraise=0.03,
      lhang=0.11,
      findent=-0.1em,
      nindent=0.2em}

    % This is for the Euler U
    \LettrineOptionsFor{U}{
      lines=3,
      loversize=0.09,
      %lraise=-0.07,
      lraise=0.01,
      lhang=0.13,
      findent=-0.4em,
      nindent=0.4em,
      slope=0.5em}

    % This is for the Euler P
    \LettrineOptionsFor{P}{
      lines=3,
      loversize=0.085,
      lraise=0.0,
      lhang=0.19,
      findent=0.0em,
      nindent=0.2em,
      slope=-1.2em}

    % This is for the Euler O
    \LettrineOptionsFor{O}{
      lines=3,
      loversize=0.05,
      lraise=0.02,
      lhang=0.09,
      findent=-0.4em,
      nindent=0.6em,
      slope=0.0em}

    % This is for the Euler V
    \LettrineOptionsFor{V}{
      lines=3,
      loversize=0.06,
      %lraise=-0.07,
      lraise=0.0,
      lhang=0.4,
      findent=0.3em,
      nindent=-0.2em,
      slope=-0.4em}

    % This is for the Euler D
    \LettrineOptionsFor{D}{
      lines=3,
      loversize=0.08,
      lraise=-0.005,
      lhang=0.12,
      findent=-0.4em,
      nindent=0.5em,
      slope=-0.2em}
  \fi
\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Acronyms.  Use \na{DFT}{density functional theory}.  This will produce an
% acronym that you can use with 
% \gls{DFT} -> "density functional theory (DFT)" or "\textsc{dft}"
% \Gls{DFT} -> "Density functional theory (DFT)" or "\textsc{Dft}"
% \glspl{DFT} -> "density functional theories (DFTs)" or "\textsc{dft}s"
% etc. with the first occurrence showing the definition.
%
% If the typeset acronym is not simply "\lowercase{\textsc{DFT}}" then you can
% provide it explicitly with the optional argument,
% i.e. \na[\textsc{d}o\textsc{e}]{DOE}{Department of Energy}
\if@aps
  \newcommand\na[3][\@empty]{%
    {%
      \ifx#1\@empty
        \lowercase{\def\short{\protect\mysc{#2}}}
      \else
        \def\short{#1}
      \fi
      \PackageError{my_paper}{%
        \protect\na\space use with aps option for acronym #1.  Replace with
        \MessageBreak
        \protect\newacronym[#2]{\short}{#3}
      }{%
        Oh dear! Something’s gone wrong.\MessageBreak
        \space \space Try typing \space <return>
        \space to proceed, ignoring \protect\na.
      }
    }
  }
\else
  \newcommand\na[3][\@empty]{%
    {%
      \ifx#1\@empty
        \lowercase{\def\short{\protect\mysc{#2}}}
      \else
        \def\short{#1}
      \fi
      \expandnext{\newacronym{#2}}{\short}{#3}
    }
  }
\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Smallcaps
%
\if@midcaps
  % Mid-caps rather than small-caps: see
  % http://tex.stackexchange.com/questions/41342
  % and http://tex.stackexchange.com/a/39839/6903

  % Not working with matplotlib!
  \newcommand\fullsc[1]{\scalebox{1.06}[1.09]{\textsc{\MakeLowercase{#1}}}}
\else
  \newcommand\fullsc[1]{\textsc{\MakeLowercase{#1}}}
\fi
\newcommand{\mysc}[1]{\fullsc{#1}}

\long\def\mmf@appendto#1#2{#1=\expandafter{\the#1#2}}
\long\def\mmf@concatto#1#2{%
  \expandafter\mmf@concataux\expandafter{\the#2}#1{\the#1}}
\long\def\mmf@concataux#1#2#3{#2=\expandafter{#3#1}}

\newtoks\mmf@sc@toks
\newtoks\mmf@toks
\let\mmfsc\fullsc
\newif\ifsc@active
\def\sc@end{\ifsc@active\mmfsc{\the\mmf@sc@toks}\sc@activefalse\fi}
\protected\def\upsc#1{%
  \mmf@toks={}%
  \sc@activefalse\@upsc#1\@nil\sc@end}
\def\@upsc#1{\ifx#1\@nil\else\@@upsc{#1}\expandafter\@upsc\fi}
\def\@@upsc#1{%
  \ifnum\catcode`#1=11\relax% Letter
    \lowercase{\mmf@appendto\mmf@toks{#1}}%
    \ifnum\uccode`#1=`#1\relax%
      \ifsc@active\else\sc@activetrue\mmf@sc@toks={}\fi%
      \mmf@concatto{\mmf@sc@toks}{\mmf@toks}%
    \else%
      \sc@end%
      \the\mmf@toks%
    \fi%
    \mmf@toks={}%
  \else%
    \mmf@appendto\mmf@toks{#1}%
  \fi%
}

% Use this instead of \textsc b/c we need to replace this with \MakeUppercase %
% for APS journals.  Use \textsc only for program names.
\if@aps
  \renewcommand{\mysc}[1]{\MakeUppercase{#1}}
  \renewcommand{\upsc}[1]{\MakeUppercase{#1}}
\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Changes to default look of revtex.  I like bold for the figure caption text
% "Figure 1." etc.
\if@aps
\else
  \renewcommand{\figurename}{Figure}
  \renewcommand{\tablename}{Table}
  \renewcommand{\fnum@figure}{\textbf{\figurename~\thefigure}}
  \renewcommand{\fnum@table}{\textbf{\tablename~\thetable}}
\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Math

%\RequirePackage{mmfmath}
\newcommand{\uvect}[1]{\ifthenelse{
    \equal{#1}{i}}{
    \hat{\bm{\i}}}{
    \ifthenelse{
      \equal{#1}{j}}{
      \hat{\bm{\j}}}{
      \hat{\mathbf{#1}}}}}
\RequirePackage{isomath}
\newcommand{\I}{\mathrm{i}}
\let\ProvideMathOperator\DeclareMathOperator
\ProvideMathOperator{\Tr}{Tr}

\newcommand\accentedsymbol[2]{\def#1{#2}}
\accentedsymbol{\vk}{\vect{k}}
\accentedsymbol{\vkappa}{\vect{\kappa}}
\accentedsymbol{\vl}{\vect{l}}
\accentedsymbol{\vp}{\vect{p}}
\accentedsymbol{\vq}{\vect{q}}
\accentedsymbol{\vx}{\vect{x}}
\accentedsymbol{\vy}{\vect{y}}
\accentedsymbol{\vf}{\vect{f}}
\let\vr\relax
\accentedsymbol{\vr}{\vect{r}}
\accentedsymbol{\vj}{\vect{\jmath}}
\accentedsymbol{\vv}{\vect{v}}
\accentedsymbol{\vgamma}{\vect{\gamma}}
\accentedsymbol{\valpha}{\vect{\alpha}}
\accentedsymbol{\vbeta}{\vect{\beta}}
\accentedsymbol{\vnabla}{\vect{\nabla}}
\accentedsymbol{\vlrnabla}{\overleftrightarrow{\bm{\nabla}}}
\accentedsymbol{\vF}{\smash{\vect{F}}}
\accentedsymbol{\oa}{\op{a}}
\accentedsymbol{\ob}{\op{b}}
\accentedsymbol{\oH}{\op{H}}
\accentedsymbol{\oK}{\op{K}}
\accentedsymbol{\oV}{\op{V}}
\accentedsymbol{\otV}{\op{\tilde{V}}}
\accentedsymbol{\psisq}{\abs{\Psi}^2}
\accentedsymbol{\mA}{\mat{A}}
\accentedsymbol{\mB}{\mat{B}}
\accentedsymbol{\mC}{\mat{C}}
\accentedsymbol{\mD}{\mat{D}}
\accentedsymbol{\mE}{\mat{E}}
\accentedsymbol{\mF}{\mat{F}}
\accentedsymbol{\mH}{\mat{H}}
\accentedsymbol{\mM}{\mat{M}}
\accentedsymbol{\mQ}{\mat{Q}}
\accentedsymbol{\mR}{\mat{R}}
\accentedsymbol{\mV}{\mat{V}}
\accentedsymbol{\mX}{\mat{X}}
\accentedsymbol{\mY}{\mat{Y}}
\accentedsymbol{\mDelta}{\mat{\Delta}}
\accentedsymbol{\mchi}{\mat{\chi}}
\accentedsymbol{\mlambda}{\mat{\lambda}}
\accentedsymbol{\metric}{\delta}
\accentedsymbol{\mmetric}{\mat{\delta}}
\accentedsymbol{\ml}{\mat{l}}
\accentedsymbol{\mnu}{\mat{\nu}}

\renewcommand{\d}{\mathrm{d}}
\providecommand{\abs}[1]{\lvert{#1}\rvert}
\providecommand{\norm}[1]{\lVert#1\rVert}
%\newcommand{\vect}[1]{\vec{\mathbf{#1}}}
\providecommand{\vect}[1]{\vec{#1}}
\DeclareRobustCommand{\order}{\ensuremath{\mathcal{O}}}

\ProvideMathOperator{\sech}{sech}
\providecommand{\ifinfloat}[2]{\ifnum\@floatpenalty<0\relax #1\else #2\fi}

\ProvideMathOperator\diag{diag}
\providecommand\spinsum{\!\!\!\sum_{\sigma\in\{a,b\}}\!\!\!\!}
\providecommand\intd[2]{\int\d^{#1}{#2}\;}
\providecommand\intdbar[2]{\int\frac{\d^{#1}{#2}}{(3\pi)^{#1}}\;}
\providecommand\bra[1]{\langle #1|}
\providecommand\ket[1]{|#1\rangle}
\providecommand\braket[1]{\langle #1\rangle}
\providecommand\op[1]{\hat{#1}}
\providecommand\mat[1]{\bm{#1}}
\providecommand\mR{\mat{\rho}}
\providecommand\mM{\mat{M}}
\providecommand\mH{\mat{H}}
\providecommand\tnunu{\tilde{\nu}_r^\dagger\tilde{\nu}_r}
\providecommand\ttau{\tilde{\tau}}
\providecommand\tgamma{\tilde{\gamma}}
\providecommand\tnu{\tilde{\nu}}
\providecommand\nunu{\nu_r^\dagger\nu_r}
\providecommand\sumint[2]{
  \sum\hspace{-1.2em}\int\;\frac{\d^{#1}{#2}}{(2\pi)^{#1}}\;}

% Derivatives
\providecommand\diff[2]{\frac{\d{#1}}{\d{#2}}}
\providecommand\pdiff[2]{\frac{\partial #1}{\partial #2}}

% Small matrices
\providecommand{\sm}[4]{\ensuremath{\bigl(\begin{smallmatrix} #1 & #2 \\ #3 & #4\end{smallmatrix}\bigr)}}

% No indent after sections
\makeatletter
\patchcmd{\@xsect}{\ignorespaces}{\noindent\ignorespaces}{}{}
\makeatother

%\input{tikzspark.tex}
%\newsavebox{\mmfsparkS}
%\newsavebox{\mmfsparkSS}
%\newcommand\dosparkS[1][3.0]{%
%  \sparkline(0.01,0.99)(0,1){(1 + tanh(#1*tan((2*pi*\x - pi)/2 r)))/2}}
%\savebox{\mmfsparkS}{\dosparkS[3]}
%\newcommand\sparkS{\usebox{\mmfsparkS}}
%\savebox{\mmfsparkSS}{\rlap{\dosparkS[1]}\rlap{\sparkS}\dosparkS[2*pi]}
%\newcommand\sparkSS{\usebox{\mmfsparkSS}}

\RequirePackage[mode=buildnew]{standalone}
\newcommand{\includespark}[1]{\includestandalone{sparklines/#1}}
\newcommand{\sparkS}{\includespark{sparkS}}
\newcommand{\sparkSS}{\includespark{sparkSS}}
