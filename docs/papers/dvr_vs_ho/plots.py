import os.path

import numpy as np
import scipy.io
sp = scipy
from matplotlib import pyplot as plt

import sympy
S = sympy.S

from mmf.utils import mmf_plot as mmfplt

_TINY = np.finfo(float).tiny


class DVR1D(object):
    r"""Sinc function basis for non-periodic functions over an interval
    `x0 +- L/2` with `N` points."""
    def __init__(self, N, L, x0=0.0):
        L = float(L)
        self.N = N
        self.L = L
        self.x0 = x0
        self.a = L/N
        self.n = np.arange(N)
        self.x = self.x0 + self.n*self.a - self.L/2.0 + self.a/2.0
        self.k_max = np.pi/self.a

    def H(self, V):
        """Return the Hamiltonian with the give potential."""
        _m = self.n[:, None]
        _n = self.n[None, :]
        K = 2.0*(-1)**(_m-_n)/(_m-_n)**2/self.a**2
        K[self.n, self.n] = np.pi**2/3/self.a**2
        K *= 0.5   # p^2/2/m
        V = np.diag(V(self.x))
        return K + V

    def F(self, x=None):
        """Return the DVR basis vectors"""
        if x is None:
            x_m = self.x[:, None]
        else:
            x_m = np.asarray(x)[:, None]
        x_n = self.x[None, :]
        return np.sinc((x_m-x_n)/self.a)/np.sqrt(self.a)


class DVRPeriodic(DVR1D):
    r"""Sinc function basis for periodic functions over an interval
    `x0 +- L/2` with `N` points."""
    def __init__(self, *v, **kw):
        # Small shift here for consistent abscissa
        DVR1D.__init__(self, *v, **kw)
        self.x -= self.a/2.0
        
    def H(self, V):
        """Return the Hamiltonian with the give potential."""
        _m = self.n[:, None]
        _n = self.n[None, :]
        _arg = np.pi*(_m-_n)/self.N
        if (0 == self.N % 2):
            K = 2.0*(-1)**(_m-_n)/np.sin(_arg)**2
            K[self.n, self.n] = (self.N**2 + 2.0)/3.0
        else:
            K = 2.0*(-1)**(_m-_n)*np.cos(_arg)/np.sin(_arg)**2
            K[self.n, self.n] = (self.N**2 - 1.0)/3.0
        K *= 0.5*(np.pi/self.L)**2   # p^2/2/m
        V = np.diag(V(self.x))
        return K + V

    def F(self, x=None):
        """Return the DVR basis vectors"""
        if x is None:
            x_m = self.x[:, None]
        else:
            x_m = np.asarray(x)[:, None]
        x_n = self.x[None, :]
        F = np.sinc((x_m-x_n)/self.a)/np.sinc((x_m-x_n)/self.L)/np.sqrt(self.a)
        if (0 == self.N % 2):
            F *= np.exp(-1j*np.pi*(x_m-x_n)/self.L)
        return F


class Paper(object):
    def __init__(self, aps=True, save=True):
        self.save = save
        self.aps = aps
        if aps:
            style = 'aps'
            self.dir = 'figures/aps'
        else:
            style = 'arXiv'
            self.dir = 'figures/arXiv'

        self.plot_properties = mmfplt.LaTeXPlotProperties(
            style=style)
    
    def savefig(self, fig):
        if self.save:
            if not os.path.exists(self.dir):
                os.makedirs(self.dir)
            filename = os.path.join(self.dir, fig.filename)
            fig.savefig(filename)
        
    def fig_HO(self):
        fig = mmfplt.Figure(
            filename='HO.pdf',
            width='columnwidth',
            plot_properties=self.plot_properties)

        DVR = DVRPeriodic
        plt.subplot(111)        # Needed for tight_layout() to work.
        #Ls = [30.0,40.0,50.0]
        Ns = [30, 40, 50]
        a = 1.0
        k_c = np.pi/a

        def V(x):
            r"""HO potential"""
            return w**2*x**2/2.0

        for _N in Ns:
            _L = np.pi*_N/k_c
            w = 2.0*np.pi/_L
            dvr = DVR(N=_N, L=_L)

            E = np.linalg.eigvalsh(dvr.H(V=V))
            n = np.arange(_N)
            En = (n + 0.5)*w
            plt.semilogy(n, abs(E-En)/w + 1e-16, 'b-+')

        _L = 30.0
        w = 2.0*np.pi/_L
        for _N in [60, 90]:
            a = _L / _N
            k_c = np.pi/a
            dvr = DVR(N=_N, L=_L)

            E = np.linalg.eigvalsh(dvr.H(V=V))
            n = np.arange(_N)
            En = (n + 0.5)*w
            plt.semilogy(n, abs(E-En)/w + 1e-16, 'r:.')

        plt.axis([0, 50, 1e-15, 100])
        plt.xlabel(r'$n$')
        plt.ylabel(r'$\Delta E/\hbar\omega$')
        plt.gcf().set_tight_layout(True)
        self.savefig(fig)

    def fig_ScarfII(self):
        fig = mmfplt.Figure(
            filename='ScarfII.pdf',
            width='columnwidth',
            plot_properties=self.plot_properties)

        DVR = DVRPeriodic
        plt.subplot(111)        # Needed for tight_layout() to work.

        V_scarf = S(
            '(A**2 + (-A**2 - A + B**2 + B*(2*A + 1)*sinh(x/a))/cosh(x/a)**2)' +
            '/(2*a**2*m) - A**2/2/m')
        E_scarf = S('(A**2 - (A - n)**2)/(2*a**2*m) - A**2/2/m')
        V = sympy.lambdify(['x'],
                           V_scarf.subs(dict(A=3, B=1, m=1, a=1)),
                           'numpy')
        En = sympy.lambdify(['n'],
                            E_scarf.subs(dict(A=3, B=1, m=1, a=1)),
                            'numpy')

        ls = [':', '-.', '--', '-']
        cs = ['k', 'b', 'g']
        ylim = [1e-15, 100]

        # First hold L_max fixed to demonstrate UV convergence
        plt.subplot(121)
        L_max = 35.0
        ks = [5.0, 10.0, 15.0, 20.0]
        for k_max, _ls in zip(ks, ls):
            err = []
            Ls = []
            N_max = int(np.ceil(L_max/np.pi*k_max))
            for _N in np.arange(4, N_max, 4):
                _L = np.pi*_N/k_max
                dvr = DVR(N=_N, L=_L)
                E = np.linalg.eigvalsh(dvr.H(V=V))[:3]
                err.append(abs(E-En(np.arange(3))))
                Ls.append(_L)
            for _i, _e in enumerate(np.asarray(err).T):
                plt.semilogy(Ls, _e, ls=_ls, c=cs[_i])

            print k_max, N_max

        plt.xlabel("L")
        plt.ylabel(r'$\Delta E$')
        plt.ylim(ylim)
        ax = plt.gca()
        ax.text(0.8, 0.9, "IR", transform=ax.transAxes)
        # Hide last label
        plt.setp([plt.gca().xaxis.get_ticklabels()[-1]], visible=False)

        # Now hold k_max fixed to demonstrate IR Convergence
        plt.subplot(122)
        plt.subplots_adjust(wspace=0.0)
        k_max = 20.0
        Ls = [5.0, 15.0, 25.0, 35.0]
        for _L, _ls in zip(Ls, ls):
            err = []
            ks = []
            N_max = int(np.ceil(k_max*_L/np.pi))
            for _N in np.arange(4, N_max, 2):
                k_max = np.pi/_L*_N
                ks.append(k_max)
                dvr = DVR(N=_N, L=_L)
                E = np.linalg.eigvalsh(dvr.H(V=V))[:3]
                err.append(abs(E-En(np.arange(3))))
            for _i, _e in enumerate(np.asarray(err).T):
                plt.semilogy(ks, _e, ls=_ls, c=cs[_i])

        plt.xlabel(r'$k_c$')
        plt.ylim(ylim)
        ax = plt.gca()
        ax.text(0.8, 0.9, "UV", transform=ax.transAxes)
        plt.setp(ax.yaxis.get_ticklabels(), visible=False)
        plt.gcf().set_tight_layout(True)
        plt.subplots_adjust(wspace=0.0)

        self.savefig(fig)

    def fig_shell(self, num=1, mat_filename=None):
        # dxs, Ns, v0
        E3_2G_Fourier = np.array(
            [[[ 14.47671183,  17.38102145,  18.53469254,  12.49982362],
              [ -2.92866159,  -7.04774001, -12.1067168 , -37.61948824],
              [ -8.55358813, -14.41236779, -20.82262048, -49.69103672],
              [ -9.40244729, -15.14982577, -21.30337417, -48.68572823],
              [ -8.59299447, -13.70221542, -19.18612815, -44.09678714]],
             [[ -8.41765721, -14.20411356, -20.54078674, -49.1052139 ],
              [ -8.99994978, -14.40490896, -20.19167663, -46.16170392],
              [ -7.3285197 , -11.73527298, -16.5496619 , -39.43621719],
              [ -5.6574432 ,  -9.25939146, -13.38834127, -34.99835164],
              [ -4.37720526,  -7.43690426, -11.19605409, -32.80553097]],
             [[ -8.40278706, -13.42474129, -18.82608535, -43.45171147],
              [ -6.13620771,  -9.96070374, -14.26831193, -36.098668  ],
              [ -4.36440472,  -7.42055862, -11.17819528, -32.79415111],
              [ -3.1967295 ,  -5.82161387,  -9.41287102, -31.6432879 ],
              [ -2.42978461,  -4.82940185,  -8.46656716, -31.30243391]],
             [[ -6.08480112,  -9.88814359, -14.1790375 , -36.00471907],
              [ -4.0098176 ,  -6.92689325, -10.61025561, -32.3633685 ],
              [ -2.77005135,  -5.25886242,  -8.85024638, -31.41880921],
              [ -2.02186308,  -4.34020427,  -8.0763014 , -31.23290956],
              [ -1.54906077,  -3.85363904,  -7.78162023, -31.20499476]],
             [[ -4.32588968,  -7.36130827, -11.0929545 , -32.48675845],
              [ -2.76580066,  -5.24674864,  -8.81693797, -31.09766392],
              [ -1.90924783,  -4.20736087,  -7.95815798, -30.89090835],
              [ -1.40923945,  -3.72759692,  -7.6930509 , -30.87071745],
              [ -1.09971799,  -3.52748395,  -7.62156784, -30.87027854]]])

        E3_2G_Dirichlet = np.array(
            [[[2.28033557e+02, 2.66605163e+02, 3.00359677e+02, 3.97317826e+02],
              [1.37345759e+02, 1.53260543e+02, 1.64582971e+02, 1.80561859e+02],
              [8.38496876e+01, 8.75456762e+01, 8.80078032e+01, 7.23428910e+01],
              [5.23663497e+01, 5.03836088e+01, 4.63376589e+01, 1.99909494e+01],
              [3.36474623e+01, 2.92673084e+01, 2.35628622e+01,-5.60938968e+00]],
             [[8.38448294e+01, 8.75327235e+01, 8.79833300e+01, 7.22506489e+01],
              [4.18157947e+01, 3.83587508e+01, 3.32563927e+01, 4.91751135e+00],
              [2.22618410e+01, 1.70054170e+01, 1.08451228e+01,-1.83125142e+01],
              [1.26034748e+01, 7.18426303e+00, 1.15400906e+00,-2.65238610e+01],
              [7.50416468e+00, 2.37375408e+00,-3.27767335e+00,-2.94550449e+01]],
             [[3.36343064e+01, 2.92444218e+01, 2.35292609e+01,-5.69349682e+00],
              [1.51387326e+01, 9.68989850e+00, 3.56328694e+00,-2.46697818e+01],
              [7.50376122e+00, 2.37333439e+00,-3.27808294e+00,-2.94590245e+01],
              [3.97414670e+00,-6.64486057e-01,-5.83836610e+00,-3.06697012e+01],
              [2.17709670e+00,-2.03534297e+00,-6.85812636e+00,-3.09662594e+01]],
             [[1.51320159e+01, 9.68104410e+00, 3.55614909e+00,-2.46310111e+01],
              [6.36699663e+00, 1.35880263e+00,-4.16160950e+00,-2.98896138e+01],
              [2.93354790e+00,-1.48100563e+00,-6.46224408e+00,-3.08294948e+01],
              [1.38463623e+00,-2.57194716e+00,-7.20780761e+00,-3.09879130e+01],
              [6.06708007e-01,-3.02527715e+00,-7.45873026e+00,-3.10132349e+01]],
             [[7.48700795e+00, 2.33042244e+00,-3.35345077e+00,-2.96730161e+01],
              [2.92518994e+00,-1.50982971e+00,-6.52399511e+00,-3.11312360e+01],
              [1.18006276e+00,-2.72111669e+00,-7.34192700e+00,-3.13546187e+01],
              [4.02657782e-01,-3.14786308e+00,-7.56579248e+00,-3.14215888e+01],
              [1.67331119e-02,-3.30798864e+00,-7.62920655e+00,-3.14640828e+01]]
         ])

        E3_PT_Fourier = np.array(
            [[[-32.80786666, -51.00989378, -70.67215412,-167.18521238],
              [-22.94977793, -37.06062261, -53.70717886,-151.40642735],
              [-16.58765069, -28.35131658, -44.11782897,-147.4629962 ],
              [-12.41627883, -22.97958442, -39.28105007,-146.6908667 ],
              [ -9.61130739, -19.75790842, -37.16837098,-146.55391053]],
             [[-16.48843674, -28.22973416, -44.00620253,-147.52513815],
              [-10.8491056 , -21.12237168, -37.97287265,-146.68094281],
              [ -7.66307664, -17.91866221, -36.34151959,-146.62005485],
              [ -5.76170593, -16.66439861, -35.96813376,-146.61654244],
              [ -4.57513232, -16.2264719 , -35.88580556,-146.61668166]],
             [[ -9.56159543, -19.72874031, -37.26211037,-149.14705313],
              [ -6.28941326, -16.97223434, -36.16775075,-149.17409859],
              [ -4.57429608, -16.25573943, -36.02098987,-149.20216867],
              [ -3.62435246, -16.08859123, -36.00199241,-149.22002813],
              [ -3.0906516 , -16.0497142 , -35.99980361,-149.23189959]],
             [[ -6.29695091, -17.2597323 , -37.41112703,-163.97299404],
              [ -4.30011408, -16.5262186 , -37.29940625,-164.3266443 ],
              [ -3.34114416, -16.41206863, -37.30126408,-164.52626567],
              [ -2.88214534, -16.39492594, -37.30789822,-164.64931719],
              [ -2.67463597, -16.39290086, -37.31275033,-164.73030321]],
             [[ -4.68541439, -18.19991957, -42.7379879 ,-198.88322423],
              [ -3.46528787, -18.1036894 , -42.85146811,-199.78503949],
              [ -2.98114182, -18.10809893, -42.91960971,-200.28538975],
              [ -2.80610933, -18.11722764, -42.96175905,-200.5907664 ],
              [ -2.74642394, -18.12397125, -42.98950157,-200.79046783]]])

        E3_PT_Dirichlet = np.array(
            [[[ 72.4427079 ,  40.57627454,   8.06304747,-128.1975571 ],
              [ 35.96244234,   9.19708383, -18.77178077,-141.98715568],
              [ 18.94685986,  -4.08235751, -28.95149584,-145.15058694],
              [ 10.21723414, -10.12236926, -32.96605146,-145.80296243],
              [  5.40796359, -13.00224469, -34.5687906 ,-145.92529995]],
             [[ 18.94217497,  -4.08754652, -28.9561942 ,-145.13947261],
              [  7.47469095, -11.82019417, -33.94848243,-145.87027888],
              [  2.60629572, -14.41808882, -35.20626709,-145.92293707],
              [  0.3018972 , -15.33680378, -35.51783046,-145.92250419],
              [ -0.87876181, -15.66908785, -35.59312166,-145.91888973]],
             [[  5.40737689, -12.99548219, -34.5386484 ,-145.29618586],
              [  0.90132174, -15.11767943, -35.41903847,-145.15121104],
              [ -0.8773663 , -15.65802975, -35.54702943,-145.01546434],
              [ -1.66293668, -15.79748232, -35.56029513,-144.90528393],
              [ -2.03723454, -15.83285963, -35.55742191,-144.81496413]],
             [[  0.9195912 , -15.01901752, -35.08961099,-141.30092019],
              [ -1.11621699, -15.59723129, -35.15409306,-140.4838322 ],
              [ -1.86501142, -15.69019219, -35.10070561,-139.87343314],
              [ -2.17069583, -15.69509645, -35.04819934,-139.40294574],
              [ -2.3035435 , -15.68564409, -35.00462107,-139.03016235]],
             [[ -0.77939311, -15.13297911, -33.94558709,-131.6443506 ],
              [ -1.78637644, -15.21523127, -33.69574178,-129.92305196],
              [ -2.12657415, -15.1658821 , -33.49071754,-128.6904675 ],
              [ -2.25066166, -15.11119677, -33.33177837,-127.76665659],
              [ -2.29661982, -15.06516058, -33.20597416,-127.04940215]]])

        E4_2G_Fourier = np.array(
            [[[230.68998645,  336.75080555,  437.71307604,  803.32224227],
              [ 91.16514862,  124.87594127,  153.66059008,  237.32058263],
              [ 20.18454226,   22.25599367,   21.48042815,    1.68557346]],
             [[ 83.5783236 ,  113.50565091,  138.63772257,  208.86628435],
              [  2.12810269,   -2.83777   ,   -9.84874491,  -49.56235791],
              [-15.56649473,  -25.97310899,  -37.30514764,  -88.56878377]],
             [[ 16.32039749,   16.64593972,   14.17523441,  -12.42298516],
              [-15.30452755,  -25.56716458,  -36.75273485,  -87.38032109],
              [-14.87355315,  -23.85330022,  -33.64822337,  -79.94252729]],
             [[ -7.63975027,  -15.81503765,  -25.46083216,  -72.66185118],
              [-15.45949553,  -24.81211376,  -34.94580449,  -82.0573227 ],
              [-11.08311865,  -18.40879974,  -26.9748373 ,  -72.13767981]],
             [[-14.44200721,  -24.12484566,  -34.66727655,  -82.36099279],
              [-12.71963386,  -20.64845109,  -29.5474664 ,  -74.1048849 ],
              [ -8.31088729,  -14.84176234,  -23.12168322,  -68.82567813]]])

        E4_2G_Dirichlet = np.array(
            [[[1160.37007192,  1354.84176292,  1547.77057297,  2303.75502302],
              [ 612.16216491,   735.12863298,   850.1163583 ,  1242.11044854],
              [ 349.83016137,   407.56914274,   454.814668  ,   570.73493005]],
             [[ 614.3598004 ,   739.47781765,   857.49106517,  1269.74395891],
              [ 265.76620687,   301.20143595,   327.00429371,   369.32553393],
              [ 118.4720379 ,   119.61986564,   115.57357687,    73.23399908]],
             [[ 352.14518633,   412.20091515,   462.26610452,   591.28572565],
              [ 118.44950642,   119.54000712,   115.40750604,    72.49693614],
              [  43.82693343,    34.42443564,    22.97437731,   -32.85998255]],
             [[ 202.72960592,   222.42961241,   233.74542323,   230.65745511],
              [  55.60852304,    47.24540693,    36.36749135,   -19.15405069],
              [  17.60511354,     7.30519862,    -4.14338032,   -56.89159275]],
             [[ 117.986951  ,   118.39442669,   113.31725143,    65.90270595],
              [  27.47938909,    17.09612751,     5.26775309,   -49.63169973],
              [   7.13837335,    -2.50373269,   -13.16930176,   -63.23230094]]])

        E4_PT_Fourier = np.array(
            [[[-133.21274609, -201.02938635, -269.66778637, -552.50221653],
              [-91.93473555, -141.43123579, -193.50996058, -428.07346725],
              [-63.93823757, -102.32039579, -145.90029969, -370.92215993]],
             [[-89.42181093, -137.94272215, -189.25558205, -423.12160467],
              [-53.28971503,  -88.01984016, -129.71458469, -360.81730062],
              [-34.376917  ,  -64.66898534, -107.67235749, -354.143597  ]],
            [[-61.52722837,  -99.26712635, -142.91874701, -381.68999518],
             [-34.09393426,  -64.54017647, -108.48019101, -370.7038717 ],
             [-22.22583544,  -55.049885  , -103.7549462 , -371.37685769]],
             [[-44.37874169,  -77.6512829 , -123.28176895, -415.68020854],
              [-24.32908011,  -58.15872476, -111.81415212, -421.80647712],
              [-17.5716694 ,  -56.18666527, -111.80235617, -424.55665116]],
             [[-34.05756405,  -71.01377033, -132.94134423, -487.58423231],
              [-20.18723569,  -65.69788909, -134.51802148, -499.38619458],
              [-17.48657897,  -66.06708608, -135.87700409, -503.81516329]]])

        E4_PT_Dirichlet = np.array(
            [[[586.53542379,  496.12802991,  405.57465429,   41.92799754],
              [193.54556286,  118.6566904 ,   42.96816005, -267.34214968],
              [ 73.16381351,   10.79400604,  -53.56102901, -329.00183725]],
             [[193.22394045,  118.22271493,   42.44925877, -267.95793589],
              [ 44.93175982,  -12.53570948,  -72.70617996, -337.25985339],
              [  6.42398576,  -40.77497067,  -93.08750977, -342.55225723]],
             [[ 72.89430129,   10.50287688,  -53.78247778, -327.52811093],
              [  6.41156167,  -40.74676953,  -92.93244711, -339.75498033],
              [ -8.08390221,  -48.48061913,  -96.81279328, -338.90091029]],
             [[ 26.69201581,  -26.3589288 ,  -82.48945976, -330.48164128],
              [ -5.98083059,  -47.21302732,  -95.1882877 , -326.98182477],
              [-11.89401226,  -49.11728397,  -95.34937036, -323.0442498 ]],
             [[  6.8072566 ,  -39.1636846 ,  -88.96806079, -312.75391354],
              [-10.21193146,  -47.34477592,  -91.54411121, -301.45869975],
              [-12.6840457 ,  -47.34247481,  -90.25098371, -294.43716737]]])

        
        # Index order:
        # dx, N v0

        # Reorganize energies into "error bands" with Dirichlet as an upper
        # bound and Fourier as a lower bound
        E3_PT = ((E3_PT_Dirichlet + E3_PT_Fourier)/2.0 + 
                 (E3_PT_Dirichlet - E3_PT_Fourier)/2.0*1j)
        E4_PT = ((E4_PT_Dirichlet + E4_PT_Fourier)/2.0 + 
                 (E4_PT_Dirichlet - E4_PT_Fourier)/2.0*1j)
        E3_2G = ((E3_2G_Dirichlet + E3_2G_Fourier)/2.0 + 
                 (E3_2G_Dirichlet - E3_2G_Fourier)/2.0*1j)
        E4_2G = ((E4_2G_Dirichlet + E4_2G_Fourier)/2.0 + 
                 (E4_2G_Dirichlet - E4_2G_Fourier)/2.0*1j)
        assert np.all(0 <= E3_PT.imag)
        assert np.all(0 <= E4_PT.imag)
        #assert np.all(0 <= E3_2G.imag)  # Some small inversions here
        assert np.all(0 <= E4_2G.imag)

        dxs = np.array([0.5, 0.75, 1.0, 1.25, 1.5])
        Ns3 = np.array([8, 10, 12, 14, 16])
        v0s3 = np.array([1.0, 1.5, 2.0, 4.0])
        Ns4 = np.array([4, 6, 8])
        v0s4 = np.array([1.0, 1.5, 2.0, 4.0])
        Ls3 = dxs[:, None]*Ns3[None, :]
        Ls4 = dxs[:, None]*Ns4[None, :]

        if mat_filename:
            mdict = dict(E3_PT_Dirichlet=E3_PT_Dirichlet,
                         E3_PT_Fourier=E3_PT_Fourier,
                         E3_2G_Dirichlet=E3_2G_Dirichlet,
                         E3_2G_Fourier=E3_2G_Fourier,
                         E4_PT_Dirichlet=E4_PT_Dirichlet,
                         E4_PT_Fourier=E4_PT_Fourier,
                         E4_2G_Dirichlet=E4_2G_Dirichlet,
                         E4_2G_Fourier=E4_2G_Fourier,
                         dxs=dxs,
                         Ns3=Ns3,
                         Ns4=Ns4,
                         v0s=v0s3)
            sp.io.savemat(mat_filename, mdict)
            return locals()

        fig = mmfplt.Figure(num=num,
                            filename='shell.pdf',
                            width='columnwidth',
                            height=1.4,
                            plot_properties=self.plot_properties)

        plt.subplot(111)                   # Needed for tight_layout() to work.

        kw = dict(ms=1.5, lw=0.1, linestyle=':', marker='.')

        if self.aps:
            size = r"\footnotesize"
            offset=-0.008
        else:
            size = r"\scriptsize"
            offset=0.0

        def draw_band(x, y, color, 
                      alpha_min=0.0, alpha_max=0.1,
                      text_offset_x=0.002, text_offset_y=0.0,
                      x_min=0.05, x_max=0.085,
                      label=None, size=size, **kw):
            y1 = y.real + y.imag
            y2 = y.real - y.imag
            for _i in xrange(1, len(x)):
                alpha = max(alpha_min, min(alpha_max, (1.0-x[_i]/(x_max))))
                plt.fill_between(x=x[_i-1:_i+1],
                                 y1=y1[_i-1:_i+1],
                                 y2=y2[_i-1:_i+1], 
                                 color=color, 
                                 lw=0,
                                 alpha=alpha)
            #plt.errorbar(x, y.real, y.imag, color=color, 
            #     elinewidth=0.1, capsize=1.5, **kw)
            plt.plot(x, y1, color=color, **kw)
            plt.plot(x, y2, color=color, **kw)

            # Locate label using point with the minimum error
            if label:
                y0 = y.real[np.argmin(abs(y.imag.ravel()))]
                plt.text(min(x) - text_offset_x,
                         y0+text_offset_y,  
                         size + label,
                         verticalalignment='center',
                         horizontalalignment='right')
                

        # Triton
        E_PT, E_2G, Linv  = E3_PT, E3_2G, 1./Ls3
        for _dx, dx in enumerate(dxs):
            if _dx == len(dxs) - 1:
                def get_label(label): return label
            else:
                def get_label(label): return None
                
            # Triton 1*PT
            v0 = 1.0
            _v = np.where(v0s3 == v0)[0][0]
            kw['color'] = kw['markeredgecolor'] = 'b'
            draw_band(
                Linv[_dx, :], E_PT[_dx, :, _v],
                text_offset_y=0,
                label=get_label(
                    r"$%gV^{\text{``triton''}}_{\text{PT}}$" % (v0,)),
                **kw)

            # Triton 2*2G
            v0 = 2.0
            _v = np.where(v0s3 == v0)[0][0]
            kw['color'] = kw['markeredgecolor'] = 'g'
            draw_band(Linv[_dx, :], E_2G[_dx, :, _v],
                      text_offset_y=0,
                      label=get_label(
                          r"$%gV^{\text{``triton''}}_{2\text{G}}$" % (v0,)),
                      **kw)


            # Triton 4*2G
            v0 = 4.0
            _v = np.where(v0s3 == v0)[0][0]
            #kw['color'] = kw['markeredgecolor'] = 'k'
            draw_band(Linv[_dx, :], E_2G[_dx, :, _v],
                      label=get_label(
                          r"$%gV^{\text{``triton''}}_{2\text{G}}$" % (v0,)),
                      **kw)


        # Alpha
        E_PT, E_2G, Linv  = E4_PT, E4_2G, 1./Ls4
        kw['linestyle'] = '-'
        for _dx, dx in enumerate(dxs):
            if _dx == len(dxs) - 1:
                def get_label(label): return label
            else:
                def get_label(label): return None

            # Alpha PT
            v0 = 1.0
            _v = np.where(v0s4 == v0)[0][0]
            kw['color'] = kw['markeredgecolor'] = 'b'
            draw_band(Linv[_dx, :], E_PT[_dx, :, _v], 
                      x_max=0.2,
                      label=get_label(
                          r"$%gV^{\text{``}\alpha\text{''}}_{\text{PT}}$"
                          % (v0,)),
                      **kw)

            # Alpha 1.5*PT
            v0 = 1.5
            _v = np.where(v0s4 == v0)[0][0]
            kw['color'] = kw['markeredgecolor'] = 'b'
            draw_band(Linv[_dx, :], E_PT[_dx, :, _v], 
                      x_max=0.2,
                      text_offset_x=-0.001,
                      label=get_label(
                          r"$%gV^{\text{``}\alpha\text{''}}_{\text{PT}}$"
                          % (v0,)),
                      **kw)

            # Alpha 4*2G
            v0 = 4.0
            _v = np.where(v0s4 == v0)[0][0]
            kw['color'] = kw['markeredgecolor'] = 'b'
            #draw_band(Linv[_dx, :], E_2G[_dx, :, _v], 
            #          x_max=0.2,
            #          label=get_label(r"$%gV_{2\text{G}}$" % (v0,)),
            #          **kw)

        plt.axis([0, 0.15, -75, 0])
        plt.xticks([0,0.05,0.1,0.15])
        #plt.yticks([-40, -30, -20, -10, 0, 10])
        plt.xlabel(r"$L^{-1}$ (fm$^{-1}$)")
        plt.ylabel(r"$E$ (MeV)")
        plt.draw()
        plt.gcf().set_tight_layout(True)

        # Inset with potentials
        fig.new_inset_axes([0.278 + offset, 0.24, 0.2, 0.25])

        r = np.linspace(0.0, 6.0, 100.0)

        r_0 = 3.0
        hbarc = 197.326         # fm and MeV units
        m = 939.565

        # PT.
        mu = 2.0/r_0
        v0 = -4.0 * (mu * hbarc)**2 / (2 * m)
        V_PT = v0 / np.cosh(mu * r)**2

        # 2G
        mu2 = (2.0/r_0)**2
        v0 = -3.144 * mu2 * hbarc**2 / (2 * m)
        V_2G = v0 * (np.exp(-mu2*r**2/4) - 4*np.exp(-mu2*r**2))

        plt.plot(r, 4*V_2G, '-g', lw=0.7)
        plt.plot(r, 4*V_PT, '-b', lw=0.7)

        plt.yticks([-200, -100, 0, 100, 200, 300])
        plt.xticks([0, 3, 6])
        plt.text(2, 250, size + r'$4V_{2\text{G}}$', color='g')
        plt.text(3, 30, size + r'$4V_{\text{PT}}$', color='b')
        plt.legend(loc='upper right')
        self.savefig(fig)
        return locals()
