r"""Bases."""
from __future__ import division

import time

import numpy as np

from utils import numexpr, fftn, ifftn, fftfreq
import dvr


class RelCoord(object):
    r"""Some simple calculations defining the relative coordinates.
    
    Define the matrix ``A`` such that the new (i.e. Jacobi) coordinates are 
    ``R = A*r`` with the ``Npart-1`` relative coordinates first.  Various
    required relationships follow from this definition.

    Attributes
    ----------
    Ainv : array
       Inverse transformation for expressing the original
       coordinates in terms of the relative coordinates.  We use this
       to compute, for example ``r[m] - r[n]`` to express the potential
       which contains terms that are functions of differences of the original
       vectors.
    kinetic_factors : array
       The transformation specified in `A` should be orthogonal so
       that ``A*A.T`` is diagonal (we check for this and only
       store/use the diagonals).  These diagonals are the
       coefficients of the kinetic energy pieces in the new basis.
    state_factors : array
       These factors are used to form the symmetric set of differences
       used in generating the initial state::
    
          sum((r[m] - r[n])**2, m, n)/2 = sum(state_factors[n]*R[n]**2)
    """
    def __init__(self, Npart=3):
        if Npart == 2:
            self.A = np.array(
                [[   1,   -1],
                 [   0.5,  0.5]])
        elif Npart == 3:
            self.A = np.array(
                [[   1,   -1,    0,     ],
                 [   0.5,  0.5,   -1.],
                 [1./3,   1./3, 1./3]])
        elif Npart == 4:
            self.A = np.array(
                [[   1,   -1,    0,      0],
                 [   0,    0,    1.,    -1],
                 [1./2, 1./2, -1./2, -1./2],
                 [1./4, 1./4,  1./4,  1./4]])
        else:
            raise NotImplementedError(
                "Only Npart<=4 (alpha particle) supported.")

        self.Ainv = np.linalg.inv(self.A)

        # Form the kinetic factors
        self.kinetic_factors = np.diag(np.dot(self.A, self.A.T))
        assert np.allclose(np.diag(self.kinetic_factors), 
                           np.dot(self.A, self.A.T))

        # Now form the factors required for the initial state
        _tmp = self.Ainv.sum(axis=0)
        _tmp = (Npart*np.dot(self.Ainv.T, self.Ainv) 
                - _tmp[:, None]*_tmp[None, :])
        self.state_factors = np.diag(_tmp)
        assert np.allclose(np.diag(self.state_factors), _tmp)
        assert np.allclose(0, self.state_factors[-1])


class Basis(object):
    r"""Presently each basis has the same x, y, and z coordinates."""

    ######################################################################
    # Require methods for subclass
    def _compute_K(self, hbarc, mc2, model):
        r"""Compute Ekin as needed."""
        raise NotImplementedError

    def apply_K(self, Phi):
        r"""Return `K(Phi)`."""
        raise NotImplementedError

    x = NotImplemented

    ######################################################################
    # Public methods
    def __init__(self, N, L, model, dim=3, disp=0, theta_bloch=0.0, **kw):
        self.dtype = model.dtype
        self.disp = disp
        self.dim = dim
        self.Npart = len(N) + 1
        self.N = np.asarray(N)
        self.L = np.asarray(L)
        self.dx = self.L/self.N
        self.metric = np.prod(self.dx)**(self.dim)
        self.shape = np.array([self.N]*self.dim).T.ravel()
        self.coord = RelCoord(self.Npart)
        self.theta_bloch = theta_bloch
        if theta_bloch != 0:
            # Ensure that basis supports Bloch momenta
            _tmp = self.k_bloch

        d_c_inds = [(_d, _c, self.inds(_d, _c)) 
                    for _d in xrange(self.dim)
                    for _c in xrange(self.Npart - 1)]

        # Compute the relative coordinates with appropriate
        # broadcasting dimensions
        R = np.empty((self.Npart - 1, self.dim), dtype=object)  # Rel. coords
        for _n, (_d, _c, _inds) in enumerate(d_c_inds):
            R[_c, _d] = self.x[_c][_inds]

        self._R = R

    @property
    def k_bloch(self):
        r"""Bloch momenta."""
        raise NotImplementedError(
            "This basis does not support twisted boundary conditions " +
            "(got theta_bloch=%s" % (str(self.theta_bloch),))
        
    def compute_matrices(self, hbarc, mc2, model):
        r"""Construct the basis."""
        tic = time.time()

        self._compute_K(hbarc=hbarc, mc2=mc2, model=model)

        if 1 < self.disp: print("Ekin took %gs" % (time.time() - tic),)

        self._compute_V(model=model)
           
        if 1 < self.disp: print("Ekin + Epot took %gs" % (time.time() - tic),)

        Phi = self._compute_Phi(model=model)

        if 1 < self.disp:
            print("Ekin + Epot + Phi took %gs" % (time.time() - tic),)

        return Phi

    def braket(self, a, b):
        return np.dot(a.conj().ravel(), b.ravel()) * self.metric

    def snorm(self, Phi):
        """Return the sqrt of the norm of Phi."""
        return np.linalg.norm(Phi.ravel()) * np.sqrt(self.metric)

    def inds(self, dimension, coordinate):
        r"""Returns a slice operator corresponding to the sepcified relative
        coordinate and the specified dimension (x=0, y=1, or z=2).  Use this to
        broadcast the abscissa or momenta for filling in the various matrices.
        """
        inds = np.array([[None]*(self.Npart-1)]*self.dim)
        inds[dimension, coordinate] = slice(None)
        inds = tuple(inds.ravel())
        return inds        

    ######################################################################
    # Private helpers
    def _R2(self, m):
        r"""Return the magnitude squared of the m'th relative
        coordinate `R[m]`."""
        return sum((self._R[m, _d])**2 
                   for _d in xrange(self.dim)).astype(self.dtype)

    def _compute_V(self, model):
        r"""Compute the potential matrices"""
        _file = model._file

        # Original coordinates
        r = np.dot(self.coord.Ainv[:, :self.Npart-1], self._R) 
                
        # All calculations done in this _tmp array to avoid memory
        # overhead.
        _tmp = np.empty(self.shape, dtype=self.dtype)
        def r2(m, n, _tmp=_tmp):
            r"""Return the square of r[m] - r[n]."""
            _tmp[::] = 0.0
            for _d in xrange(self.dim):
                _tmp += (r[m, _d] - r[n, _d])**2
            return _tmp

        if not _file or 'Epot' not in _file:
            # Potential.  This could be big.  We use it as a tmp array
            V = model.V
            Epot = np.zeros(self.shape, dtype=self.dtype)
            _tmp = np.empty(self.shape, dtype=self.dtype)

            # Sum over all pairs.
            for _m in xrange(self.Npart):
                for _n in xrange(_m+1, self.Npart):
                    Epot += V(r2=r2(_m,_n), tmp=_tmp)
            del _tmp
            
            if _file:
                model['Epot'] = Epot
                Epot = model['Epot']
        else:
            Epot = model['Epot']
        self.Epot = Epot

    def _compute_Phi(self, model):
        _file = model._file
        _R2 = self._R2
        if not _file or 'Phi' not in _file:
            _Rs = [self.coord.state_factors[_n]*_R2(_n) 
                   for _n in xrange(self.Npart-1)]
            if numexpr:
                # numexpr does not support iterating over lists and
                # since each element has a different broadcast
                # structure, it would not work to make them into a
                # single huge array, so we have to dynamically
                # construct the expression and argument list.
                _names = ['R%i' % (_n,) for _n in xrange(self.Npart-1)]
                _expr = 'exp(-ir2*(%s)/2)' % ('+'.join(_names),)
                _args = dict(zip(_names, _Rs), ir2=model.ir2)
                Phi = numexpr.evaluate(_expr, _args)
            else:
                Phi = np.exp(-model.ir2*sum(_Rs), out=Phi)

            Phi /= self.snorm(Phi)

            if _file:
                model['Phi'] = Phi
                Phi = model['Phi']
        else:
            Phi = model['Phi']
        return Phi


class Fourier(Basis):
    @property
    def k_bloch(self):
        r"""Bloch momenta."""
        return self.theta_bloch/self.L

    @property
    def x(self):
        # The use of ceil here allows one to use odd N including 0.
        return [self.dx[_c]*np.arange(np.ceil(-self.N[_c]/2.0), self.N[_c]/2.0)
                for _c in xrange(self.Npart - 1)]

    @property
    def k(self):
        return [2.0*np.pi*fftfreq(self.N[_c], d=self.dx[_c])
                for _c in xrange(self.Npart - 1)]

    def _compute_K(self, hbarc, mc2, model):
        r"""Compute the kinetic energy matrices"""
        _file = model._file
        d_c_inds = [(_d, _c, self.inds(_d, _c)) 
                    for _d in xrange(self.dim)
                    for _c in xrange(self.Npart - 1)]

        if not _file or 'Ekin' not in _file:
            # Kinetic energy.  This could be big.
            Ekin = np.zeros(self.shape, dtype=self.dtype)
            for _n, (_d, _c, _inds) in enumerate(d_c_inds):
                Ekin += (self.coord.kinetic_factors[_c]
                         * ((self.k[_c] - self.k_bloch[_c])**2)[_inds]
                         * hbarc**2 / (2*mc2))
            if _file:
                model['Ekin'] = Ekin
                Ekin = model['Ekin']         # Allow array to be deleted
        else:
            Ekin = model['Ekin']

        self.Ekin = Ekin

    def apply_K(self, Phi):
        # Phi could be on disk... make it an array
        Phi = np.asarray(Phi, dtype=self.dtype).reshape(self.shape)
        KPhi = fftn(Phi)
        KPhi *= self.Ekin               # Allows Ekin to be on a file
        KPhi = ifftn(KPhi).real
        return KPhi


class DVR(Basis):
    DVR = NotImplemented
    
    @property
    def x(self):
        return [self.DVR(N=self.N[_c], L=self.L[_c], x0=0.0).x
                for _c in xrange(self.Npart-1)]
        
    def _compute_K(self, hbarc, mc2, model):
        metric = 1.0
        Ks = []
        for _c in xrange(self.Npart-1):
            dvr = self.DVR(N=self.N[_c], L=self.L[_c], x0=0.0)
            metric *= dvr.metric**(self.dim)
            Ks.append(dvr.K*hbarc**2/mc2 * self.coord.kinetic_factors[_c])
        self._Ks = Ks
            
    def apply_K(self, Phi):
        # Phi could be on disk... make it an array
        Phi = np.asarray(Phi, dtype=self.dtype).reshape(self.shape)
        Ks = self._Ks
        KPhi = 0*Phi
        for _i, (_c, _d) in enumerate((__c, __d) 
                                      for __c in xrange(self.Npart-1)
                                      for __d in xrange(self.dim)):
            KPhi += np.rollaxis(np.tensordot(Ks[_c], Phi, axes=([1,_i])),
                                0, _i + 1)
        return KPhi


class Sinc(DVR):
    DVR = dvr.Sinc


class Dirichlet(DVR):
    DVR = dvr.Dirichlet
