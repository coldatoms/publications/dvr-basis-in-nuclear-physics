r"""Lanczos methods for finding the lowest `n` eigenvalues of a matrix.
"""

from __future__ import division
__all__ = ['lanczos', 'lanczos_gen']

import math
import numpy.linalg
np = numpy


def lanczos(x, apply_H, braket, k=10):
    r"""Use a non-orthogonalized version of Lanczos as a generator expression.

    Examples
    --------
    >>> N = 100
    >>> k = 50
    >>> H = np.diag(np.arange(N)) + np.ones((N, N))
    >>> def apply_H(x): return np.dot(H, x)
    >>> def braket(a, b): return np.dot(a, b)
    >>> x = np.ones(N)
    >>> exact = np.linalg.eigvalsh(H)
    >>> approx = lanczos(x, apply_H=apply_H, braket=braket, k=k)
    >>> np.allclose(approx[:5], exact[:5], rtol=0.0015)
    True
    """
    gen = lanczos_gen(x=x, apply_H=apply_H, braket=braket)
    return [gen.next() for _n in xrange(k)][-1]


def lanczos_gen(x, apply_H, braket):
    r"""Use a non-orthogonalized version of Lanczos as a generator expression.
    """
    def norm(x):
        return math.sqrt(braket(x, x))

    a_ = []                     # Diagonal elements
    b_ = []                     # Off-diagonal elements

    v0 = x

    v0 /= norm(v0)

    f = apply_H(v0)
    alpha = braket(v0, f); a_.append(alpha)
    f -= alpha*v0
    while True:
        beta = norm(f); b_.append(beta)
        v1 = f/beta
        
        f = apply_H(v1) - v0*beta
        v0 = v1

        alpha = braket(v1, f); a_.append(alpha)
        f -= v1*alpha

        T = np.diag(a_) + np.diag(b_, k=-1)
        yield np.linalg.eigvalsh(T, UPLO='L')
    
    
