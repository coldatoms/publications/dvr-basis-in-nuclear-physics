r"""Brute force solution of the few-body problem.

This module provides the :class:`ShellModel` class which allows one to explicity
formulate and solve the few-body problem using DVR bases.

Examples:

Here we solve for some small bases just to check and benchmark the code.  These
examples should take a few seconds to runs.  Note that we choose between a
triton and an alpha particle by specifying the number of N's and Ls

>>> from shell_model import ShellModel
>>> triton = ShellModel(N=(8,)*2, L=(15,)*2, V='V_PT', v0=1.0, r_0=3.0,
...                     basis='Fourier',  disp=1)
Each matrix is 0.00195312GB
  (will need ~0.00585938GB to initialize)
  (will need ~0.0117188GB to run)

This is an estimate of the resources needed.  One can, for example, use single
precision (change ``_DTYPE``) to reduce the memory requirements.  Now we compute
the matrices (this can take some time for larger systems) 

>>> triton.compute_matrices()

Finally, compute the lowest three states using Lanczos.

>>> triton.lanczos(n=3)           # Get the three lowest states
array([-4.5863968 , -0.7126005 ,  3.05656059])

Note: this iterates until the Lanczos eigenvalues stop changing (to a specified
absolute tolerance ``atol``).  Since we do not orthogonalize the states, one
sometimes finds spurious degenerate eigenvalues... this happens if we try to
converge too many eigenvalues, and is a well-known property of the Lanczos
algorithm without re-orthogonalization.

>>> triton.lanczos(n=5)         # doctest: +ELLIPSIS
array([-4.5863... , -4.5863..., -0.7126...,  3.0565...,  3.2973... ])

To get some idea of the errors, we can use the Fourier and Dirichlet bases to
give lower and upper bounds respectively.  This can be understood as follows:
the Fourier boundary condition introduces an image system and the ground state
correspond to the zero Bloch-momentum state of the band structure.  This always
has lower energy than the isolated system.  The Dirichlet condition, on the
other hand, forces the wavefunction to zero prematurely, thereby yielding higher
energies. (These bounds are not rigorous bounds of the converged energy
due to higher order effects, but generally give a good picture of the
convergence.)

>>> def E_bound(**kw):
...     Es = []
...     s = ShellModel(disp=0, basis='Fourier', **kw)
...     s.compute_matrices()
...     Es.append(s.lanczos(n=1))
...     s = ShellModel(disp=0, basis='Dirichlet', **kw)
...     s.compute_matrices()
...     Es.append(s.lanczos(n=1))
...     return np.asarray(Es).ravel()
>>> E_bound(N=(8,)*2, L=(15,)*2, V='V_PT', v0=1.0, r_0=3.0)
array([-4.58639485, -1.45177082])
>>> E_bound(N=(8,)*2, L=(15,)*2, V='V_2G', v0=1.0, r_0=3.0)
array([-2.79276702,  2.96799152])

We now compute the bounds using twisted boundary conditions by specifying
theta_bloch:

>>> def E_bound_bloch(tB=np.pi, **kw):
...     Es = []
...     s = ShellModel(disp=0, basis='Fourier', theta_bloch=0.0, **kw)
...     s.compute_matrices()
...     Es.append(s.lanczos(n=1))
...     s = ShellModel(disp=0, basis='Fourier', theta_bloch=tB, **kw)
...     s.compute_matrices()
...     Es.append(s.lanczos(n=1))
...     return np.asarray(Es).ravel()


Here is a similar analysis for the alpha particle.  This is much further from
convergence due to the smaller lattice size:

>>> E_bound(N=(4,)*3, L=(10,)*3, V='V_PT', v0=1.0, r_0=3.0)
array([-71.7706519 ,  -6.50192494])
>>> E_bound(N=(4,)*3, L=(10,)*3, V='V_2G', v0=1.0, r_0=3.0)
array([-10.97039707,  18.4955477 ])


"""
from __future__ import division

import sys
import time
import warnings

import numpy as np

import solvers
import bases
from utils import numexpr, fftn, ifftn

_DTYPE = np.float64                     # Default is single precision.


class ShellModel(object):
    r"""Shell model calculation of a mock "alpha" particle with a double
    Gaussian interaction.
    
    We use a periodic DVR basis in a set of `N-1` relative coordinates.
    """
    # units fm and MeV
    hbarc = 197.326
    m_nc2 = 939.565
    m_pc2 = 938.272
    Npart = 4                           # Alpha particle

    def __init__(self, dim=3, N=(4,)*(Npart-1), L=(10.0,)*(Npart-1), 
                 filename=None,
                 r_0=6.0, v0=2.0, disp=2,
                 V='V_PT',
                 basis='Dirichlet',
                 theta_bloch=0,
                 dtype=_DTYPE, limit_memory=True):
        self.Vname = V
        self.dim = dim
        self.Npart = len(N) + 1
        self.N = np.asarray(N)
        self.L = np.asarray(L)
        self.dx = self.L/self.N
        self.Ec = self.hbarc**2 * np.pi**2 / (2 * self.m_nc2 * self.dx**2)
        self.Lambda = self.hbarc*np.pi/self.dx
        self.tau = 0.0
        self.dtype = dtype

        self.r_0 = r_0      # Effective range of PT potential at unitarity.
        self.v0 = v0        # Strength of potential (1 = unitary)
        
        self.ir2 = 0.5/self.r_0**2
        self.limit_memory = limit_memory
        self.disp = disp
        
        if 1 < self.disp:
            print np.array((self.L, self.N, self.dx, self.Ec, self.Lambda))

        # one-dimensional coordinate and momentum vectors
        Basis = getattr(bases, basis)
        self.basis = Basis(N=N, L=L, dim=self.dim, model=self, 
                           theta_bloch=theta_bloch)
        self.x = self.basis.x
        self.dV = self.basis.metric


        # Shape of matrices.  Dimensions correspond to a ravelled index array
        # with shape (coordinate, dimension) i.e. [1,1] is the y coordinate of
        # the second relative coordinate.
        _GB = np.prod(self.basis.shape)*self.dtype().nbytes/1024.0**3
        _f = 8
        if self.limit_memory:
            _f = 6
        if 0 < self.disp:
            print("Each matrix is %gGB" % (_GB,))
            print("  (will need ~%gGB to initialize)" % (3*_GB,))
            print("  (will need ~%gGB to run)" % (_f*_GB,))

        self._file = None               # hd5 file handle

        if filename is not None:
            import h5py                 # Only require h5py if needed

            # Need to choose a reasonable chunking strategy... the default fails
            # for N >= 10 and the simple chunks=True works, but makes the file
            # *way* too big!
            self._chunks = (1,) + tuple(self.basis.shape)[1:]
            self._file = h5py.File(filename)
            if 0 < self.disp:
                print("Hd5 file %s is open..." % self._file.filename)

    def __del__(self):
        if self._file:
            if 0 < self.disp:
                print("Closing file %s" % self._file.filename)
            self._file.close()

    def V(self, r2, tmp=None):
        return getattr(self, self.Vname)(r2, tmp=tmp)

    def V_2G(self, r2, tmp=None):
        r"""S-wave interaction tuned to unitarity.

        Note: The parameter ``r_0`` here is not the effective range: at
        unitarity one has ``r_e = r_0*0.5061...``.

        Note: r2 is destroyed (to save memory)
        """
        mu2 = self.dtype((2./self.r_0)**2)     # Old
        #mu2 = self.dtype((3.952/self.r_e)**2)  # Effective range

        U0 = self.dtype(-3.144 * self.v0 * mu2 *
                        self.hbarc**2 / (2 * self.m_nc2))
        if numexpr:
            # Be sure to use integers here: 4.0, 0.25 etc. will break float32
            # because they are interpreted as float64
            return numexpr.evaluate('U0*(exp(-mu2*r2/4) - 4*exp(-mu2*r2))',
                                    dict(U0=U0, mu2=mu2.astype(self.dtype),
                                         r2=r2.astype(self.dtype)), out=tmp)
        else:
            mu2r2 = r2
            neg_mu2r2 *= -mu2
            tmp = np.exp(neg_mu2r2, out=tmp)
            tmp *= -4.0 
            neg_mu2r *= 4.0
            tmp += np.exp(neg_mu2r, out=neg_mu2r)
            tmp *= U0
            return tmp

    def V_PT(self, r2, tmp=None):
        r"""S-wave interaction tuned to unitarity.

        Note: r2 is destroyed (to save memory)
        """
        mu = self.dtype(2./self.r_0)
        mu2 = mu*mu
        U0 = self.dtype(-4.0 * self.v0 * mu2 * self.hbarc**2 / (2 * self.m_nc2))
        if numexpr:
            # Be sure to use integers here: 4.0, 0.25 etc. will break float32
            # because they are interpreted as float64
            return numexpr.evaluate('U0/cosh(mu*sqrt(r2))**2',
                                    dict(U0=U0, mu=mu.astype(self.dtype),
                                         r2=r2.astype(self.dtype)), out=tmp)
        else:
            return -4.0*U0*np.sech(mu*np.sqrt(r2))

    def compute_matrices(self):
        self.Phi = self.basis.compute_matrices(
            hbarc=self.hbarc, mc2=self.m_nc2, model=self)

    def plot_V(self):
        plt.figure(1)
        r = np.linspace(0, 10, 100)
        plt.plot(r, V(r2=r**2));
        plt.xlabel('r [fm]')
        plt.ylabel('U(r) [MeV]')

    def energy(self, Phi):
        """Return the energy of Phi"""
        Phi = np.asarray(Phi, dtype=self.dtype)  # Could be on disk
        KPhi = self.basis.apply_K(Phi).real
        if numexpr:
            return numexpr.evaluate(
                'sum(Phi*(KPhi + Phi*Epot))',
                dict(Phi=Phi, KPhi=KPhi, Epot=self.basis.Epot)
            )*self.basis.metric
        else:
            KPhi *= Phi
            return (KPhi + Phi**2*self.basis.Epot).sum()*self.basis.metric
        
    def apply_H(self, Phi):
        r"""Apply Hamiltonian to vector v."""
        Phi = np.asarray(Phi, dtype=self.dtype).reshape(self.basis.shape)
        KPhi = self.basis.apply_K(Phi)
        
        if numexpr:
            return numexpr.evaluate(
                'KPhi + Phi*Epot',
                dict(Phi=Phi, KPhi=KPhi, Epot=self.basis.Epot))
        else:
            return (KPhi + Phi*self.basis.Epot)

    def lanczos(self, Phi=None, n=3, atol=1e-6):
        if Phi is None:
            Phi = self.Phi
        _gen = solvers.lanczos_gen(x=Phi, apply_H=self.apply_H,
                                   braket=self.basis.braket)
        res = []
        _i = 0
        for _n in xrange(n+1):
            res.append(_gen.next())
            if 2 < self.disp:
                print len(res), res[-1][:n]
        while atol < abs(res[-1][:n] - res[-2][:n]).max():
            res.append(_gen.next())
            if 2 < self.disp:
                print len(res), res[-1][:n]
        return res[-1][:n]

    def evolve(self, Phi=None, dtau_Ec=5, nsteps=10):
        """Imaginary time evolution to find the ground state.

        `self.Phi` will be set to the current value.
        """
        ts = []
        Es = []
        if Phi is None:
            Phi = self.Phi
            ts = getattr(self, 'ts', ts)
            Es = getattr(self, 'Es', Es)

        Phi = np.asarray(Phi, dtype=self.dtype)   # Must not be on disk
        
        dtau = dtau_Ec*self.hbarc/self.Ec.max()   # step in imaginary time
        
        tauh = dtau/self.hbarc
        if 0 < self.disp:
            print("dtau=%g" % (dtau,))

        Ekin = np.asarray(self.basis.Ekin, 
                          dtype=self.dtype)    # Ekin may be on disk
        if self.limit_memory:
            Ekin *= -tauh
            exp_K = np.exp(Ekin, out=Ekin)
        else:
            exp_K = np.exp(-tauh*Ekin)
        del Ekin

        Epot = np.asarray(self.basis.Epot, 
                          dtype=self.dtype)    # Epot may be on disk
        if self.limit_memory:
            Epot *= -tauh/2
            exp_V_2 = np.exp(Epot, out=Epot)
        else:
            exp_V_2 = np.exp(-Epot * tauh/2)
        del Epot

        if not ts and not self.limit_memory:
            ts.append(self.tau)
            Es.append(self.energy(Phi))

        tic = time.time()
        Phi *= exp_V_2          # First step
        for it in xrange(nsteps):
            # Split operator method.
            _tmp = fftn(Phi)
            del Phi
            _tmp *= exp_K
            _tmp = ifftn(_tmp)
            assert np.allclose(_tmp.imag, 0)            
            Phi = _tmp.real.copy()
            del _tmp
            
            self.tau += it*dtau
            if self.limit_memory:
                Phi *= exp_V_2
                if it < nsteps - 1:
                    Phi *= exp_V_2
                else:
                    Phi /= self.basis.snorm(Phi)
                if 1 < self.disp:
                    print np.asarray((it, time.time() - tic))
            else:
                Phi *= exp_V_2
                Phi /= self.basis.snorm(Phi)
                self.Phi = Phi
                ts.append(self.tau)
                Es.append(self.energy(Phi))
                if it < nsteps - 1:
                    Phi *= exp_V_2
                if 1 < self.disp:
                    print np.asarray((it, time.time() - tic, Es[-1]))

        if self.limit_memory:
            # Need to recompute E_kin etc. and then compute energy
            del exp_K, exp_V_2
            self.compute_matrices()
            ts.append(self.tau)
            Es.append(self.energy(Phi))
            self.Phi = Phi
            if 0 < self.disp:
                print("Final Energy: %gMeV" % (Es[-1]))
            
        self.ts = ts
        self.Es = Es
        if self._file:
            f = self._file
            if 'ts' in self._file and 'Es' in self._file:
                self.ts = np.concatenate([self['ts'], ts])
                self.Es = np.concatenate([self['Es'], Es])

            self['Phi'] = self.Phi
            self['ts'] = self.ts
            self['Es'] = self.Es

        return Phi, ts, Es

    def __getitem__(self, name):
        r"""Dictionary access to file."""
        if not self._file:
            raise IndexError ("No file specified!")
        return self._file[name]

    def __setitem__(self, name, value):
        r"""Dictionary access to file."""
        if not self._file:
            raise IndexError ("No file specified!")

        chunks = None
        value = np.asarray(value, dtype=self.dtype)
        if (len(value.shape) == len(self.basis.shape)
            and np.allclose(value.shape, self.basis.shape)):
            # Only chunk large arrays
            chunks = self._chunks
        if name in self._file:
            self._file.__delitem__(name)
        self._file.create_dataset(name, data=value, chunks=chunks)

_USAGE = """Usage: python alpha.py <N> <L> <nsteps> <dtau_Ec>\
 <limit_mem> <filename>

This will generate the file 'Ekin_Epot_Phi_<N>_<L>.hd5' with the matrices
Ekin, Epot, and Phi.  If <nsteps> are provided, then the evolver is run for
<nsteps> using the specified <dt_Ec> (default 0.1).
"""

if __name__ == '__main__':
    if 1 == len(sys.argv):
        print(_USAGE)
        exit(-1)
    argv = sys.argv[:0:-1]              # Reverse order
    N = 4
    L = 10.0
    nsteps = 0
    dtau_Ec = 0.1
    limit_memory = True
    if argv: N = int(argv.pop())
    if argv: L = float(argv.pop())
    if argv: nsteps = int(argv.pop())
    if argv: dtau_Ec = float(argv.pop())
    if argv: limit_memory = argv.pop().lower().startswith('t')
    filename = 'data/Ekin_Epot_Phi_%i_%.4g.hd5' % (N, L)
    if argv: filename = argv.pop()

    print("Computing matrices for N=%i, L=%g: saving in file '%s'" 
          % (N, L, filename))
    s = ShellModel(N=(N,)*3, L=(L,)*3, filename=filename, 
                   limit_memory=limit_memory)
    
    s.compute_matrices()
    if 0 < nsteps:
        print("Evolving for %i steps with dtau_Ec = %g" % (nsteps, dtau_Ec))
        Phi, ts, Es = s.evolve(Phi=s.Phi, nsteps=nsteps, dtau_Ec=dtau_Ec)

    if False:
        from matplotlib import pyplot as plt
        plt.figure(2)
        plt.plot(ts1, Es1, 'g')
        plt.plot(ts2, Es2, 'b')
        plt.xlabel('tau [fm/c]')
        plt.ylabel('Energy [MeV]')


