r"""A few simple DVR bases.

For simplicity, these are all implemented as one-dimensional bases.
"""
from __future__ import division

import numpy as np
import scipy.special
sp = scipy


def sinc(x):
    r"""Standard sinc function (scipy computes ``sin(pi*x)/(pi*x)``).

    Examples
    --------

    >>> sinc(0.0)
    array(1.0)
    >>> sinc(1.0)/np.sin(1.0)
    1.0
    """
    return sp.special.sinc(x/np.pi)


class Sinc(object):
    r"""Sinc function basis for Dirichlet boundary conditions
    (non-periodic).

    Examples
    --------

    Here we test an HO with length 
    >>> w = 1.2                 # HO freq
    >>> m = 3.4                 # Mass
    >>> a0 = 1./np.sqrt(m*w)    # HO length
    >>> N = 8
    >>> L = np.sqrt(2.0 * N * np.pi) * a0  # Optimal L for HO problems
    >>> dvr = Dirichlet(N=N, L=L)
    >>> x = dvr.x
    >>> V = m*w**2*np.diag(x**2)/2.0
    >>> K = dvr.K/m
    >>> H = K + V
    >>> np.round(np.linalg.eigvalsh(H)/w - 0.5, 2)
    array([ 0.  ,  1.  ,  2.  ,  3.01,  4.08,  5.11,  6.68,  7.17])
    """
    def __init__(self, N, L, x0=0.0):
        self.x0 = x0
        self.dx = L/float(N)
        self.N = N

        self.k_max = np.pi/self.dx
        self.x = self._x(n=np.arange(N))
        #z = k*x
        self.lam = self.dx
    
    def _x(self, n):
        return self.dx*(n + 0.5*(1 - self.N)) + self.x0

    def F(self, n, x):
        r"""Nth basis function."""
        return sinc(self.k_max*(x - self._x(n=n)))/np.sqrt(self.dx)
        
    @property
    def K(self):
        r"""Kinetic energy matrix (no ``hbar**2/m``)."""
        m = np.arange(self.N)[:, None]
        n = m.T
        K = (-1)**(m-n)/(m-n)**2/self.dx**2
        K[n.ravel(), n.ravel()] = np.pi**2/6.0/self.dx**2
        return K
        
    @property
    def metric(self):
        return self.lam
    

class Dirichlet(object):
    r"""Sinc function basis for Dirichlet boundary conditions
    (non-periodic).

    Examples
    --------

    Here we test an HO with length 
    >>> w = 1.2                 # HO freq
    >>> m = 3.4                 # Mass
    >>> a0 = 1./np.sqrt(m*w)    # HO length
    >>> N = 8
    >>> L = np.sqrt(2.0 * N * np.pi) * a0  # Optimal L for HO problems
    >>> dvr = Dirichlet(N=N, L=L)
    >>> x = dvr.x
    >>> V = m*w**2*np.diag(x**2)/2.0
    >>> K = dvr.K/m
    >>> H = K + V
    >>> np.round(np.linalg.eigvalsh(H)/w - 0.5, 2)
    array([ 0.  ,  1.  ,  2.  ,  3.01,  4.08,  5.11,  6.68,  7.17])
    """
    def __init__(self, N, L, x0=0.0):
        self.x0 = x0
        self.L = L
        self.dx = L/float(N)
        self.N = N

        self.k_max = np.pi/self.dx
        self.x = self._x(n=np.arange(N))
        #z = k*x
        self.lam = self.dx
    
    def _x(self, n):
        return self.L*(self._z(n) - 0.5) + self.x0

    def _z(self, n):
        r"""Bayes actual basis without scaling or shifting."""
        return (1.0 + n)/(1.0 + self.N)

    @property
    def K(self):
        r"""Kinetic energy matrix (no ``hbar**2/m``).

        Formula (27) and (28) from Baye
        """
        m = np.arange(self.N)[:, None]
        xm = self._z(m)
        n = m.T
        xn = xm.T
        pi2 = np.pi/2.0
        K = (-1)**(m-n)*pi2**2*(
            1./np.sin(pi2*(xm-xn))**2 
            - 1./np.sin(pi2*(xm+xn))**2)

        K[n.ravel(), n.ravel()] = pi2**2*(
            2./3.*(self.N + 1)**2 
            + 1./3. 
            - 1./np.sin(np.pi*xn.ravel())**2)
        return K/self.L**2
        
    @property
    def metric(self):
        return self.lam
    
