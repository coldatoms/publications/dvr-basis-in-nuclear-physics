r"""Various utilities for high-performance computations.

This module attempts to load various high-performance tools like the FFTW and
numexpr that may not be installed on a default system.
"""
from __future__ import division
__all__ = ['numexpr', 'fftn', 'ifftn']

import warnings

numexpr = False
try:
    import numexpr
except ImportError:
    warnings.warn("Could not import numexpr... performance not optimal.")


from numpy.fft import fftn, ifftn, fftfreq
try:
    from anfft import fftn, ifftn

except ImportError:
    warnings.warn("Could not import anfft... performance not optimal.")
