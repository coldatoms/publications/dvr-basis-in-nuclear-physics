r"""Generate data for plots."""
from __future__ import division

import itertools

import numpy as np

import h5py

import shell_model

class Figures(object):
    r"""Generate and plot data."""
    
    def __init__(self):
        # Generate data for the following parameters
        self.dtype = float
        self.Npart = 3
        self.dxs = np.array([0.5, 0.75, 1.0, 1.25, 1.5])
        self.Ns3 = np.array([8, 10, 12, 14, 16])
        self.Ns4 = np.array([4, 6, 8])
        self.Vs = ['V_2G', 'V_PT']
        #self.v0s = np.array([1.0, 1.5, 2.0, 2.5, 4.0])
        self.v0s = np.array([1.0, 1.5])
        self.basis = ['Fourier', 'Dirichlet']
        
        self.filename = 'data/figure.hd5'
        self._file = h5py.File(self.filename)
        print("Hd5 file %s is open..." % self._file.filename)

        self.save('dxs', self.dxs[:, None, None, None])
        self.save('Ns3', self.Ns3[None, :, None, None])
        self.save('Ns4', self.Ns4[None, :, None, None])
        self.save('v0s', self.v0s[None, None, None, :])

    def __del__(self):
        if self._file:
            print("Closing file %s" % self._file.filename)
            self._file.close()

    def save(self, name, value):
        if name in self._file:
            del self._file[name]
        self._file[name] = value

    def generate_data(self, Npart=3):
        Ns = getattr(self, "Ns%i" % (Npart, ))
        shape = map(len, (self.dxs, Ns, self.Vs, self.v0s, self.basis))
        Es = None
        _name = 'Es%i' % (Npart, )
        if _name in self._file:
            Es = self._file[_name]
            if Es.shape != shape:
                Es = None
        if Es is None:
            self.save(_name, np.zeros(shape, dtype=self.dtype))
            Es = self._file[_name]

        _iters = itertools.product(
            *[tuple(enumerate(_x)) for _x in  
                   (Ns, self.dxs, self.Vs, self.v0s, self.basis)])

        for ((_n, N), (_d, dx), (_v, V), (_v0, v0), (_b, basis)) in _iters:
            if Es[_d, _n, _v, _v0, _b] == 0:
                L = dx * N
                shell = shell_model.ShellModel(N=(N,)*(Npart-1),
                                               L=(L,)*(Npart-1),
                                               basis=basis,
                                               V=V, v0=v0, disp=0)
                shell.compute_matrices()
                Es[_d, _n, _v, _v0, _b] = shell.lanczos(n=1)
                self._file.flush()
            print(dx, N, V, v0, basis, Es[_d, _n, _v, _v0, _b])
        self.Es = np.asarray(Es)
