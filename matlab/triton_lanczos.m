%---------------------------------------------------------------------------------------------
% n-dimensional kinetic and potential energy
% minimal use of n-dimensional arrays and vectorization
%---------------------------------------------------------------------------------------------
clear all
format short g
format compact
il = zeros(3,5);
en = il;
idx = 0;
for dx = 0.5:0.25:1.5
idx = idx + 1;
iNx = 0;
for Nx = 8:2:16
    iNx = iNx + 1;
%---------------------------------------------------------------------------------------------
Lx  = Nx*dx;
Ly  = Lx;
Lz  = Lx;
%Nx  = 8;  
Ny  = Nx;
%dx  = Lx/Nx;
dy  = Ly/Ny;
x   = dx*(-Nx/2:Nx/2-1);
y   = dy*(-Ny/2:Ny/2-1);
dV  = (dx*dx)^3;
%---------------------------------------------------------------------------------------------
hbarc   = 197.326;
m_nc2   = 939.565;
m_pc2   = 938.272;

% ipot   = 1;   % inverse cosh potential
ipot   = 2;   % two gaussians
% ipot   = 3;   % two exponetials
r0     = 1.5; % radius of the potential
mu     = 1/r0;
mu2    = mu^2;
if ipot == 1
    U0     =     -4*mu2*hbarc^2/(2*m_nc2)*1;
elseif ipot == 2
    U0     = -3.144*mu2*hbarc^2/(2*m_nc2)*4;
elseif ipot == 3
    U0     = -4.764*mu2*hbarc^2/(2*m_nc2);
end
%---------------------------------------------------------------------------------------------
% potentials taken from Forbes, Gandolfi, and Gezerlis, Phys.Rev. A 86, 053603 (2012)
%%---------------------------------------------------------------------------------------------
ir2    = mu2/2; % radius of initial wave function
Lambda = hbarc*pi/dx;
Ec     = Lambda^2/(2*m_nc2);
disp([Nx, Ny,(Nx*Ny)^3]) 
disp([Lx, Ly, dx, dy])
disp([ipot, U0, mu, Ec, Lambda])
tic
% coordinates
% --------------------------
% X =  r1 - r2
% Y = (r1 + r2 - 2 r3)/2
% R = (r1 + r2 +   r3 )/3
% --------------------------
% r1 =   X/2 +   Y/3 + R
% r2 =  -X/2 +   Y/3 + R
% r3 =       - 2 Y/3 + R
% --------------------------
% r1 - r2 =  X
% r1 - r3 =  X/2 + Y 
% r2 - r3 = -X/2 + Y 
% --------------------------
%------------------------------------------------------------------------
% k1*r1 + k2*r2 + k3*r3  = 
%     (k1 - k2)/2*X + (k1 + k2 - 2 k3)/3*Y + (k1 + k2 + k3)*R 
%------------------------------------------------------------------------
% units fm and MeV
% Potential energy and intial wave function
%---------------------------------------------------------------------------------------------
rr   = zeros(Nx,Nx,Nx,Ny,Ny,Ny);
% r12
for ix = 1:Nx
    for iy = 1:Nx
        for iz = 1:Nx
          rr(ix,iy,iz,:,:,:) = rr(ix,iy,iz,:,:,:) + (x(ix)^2+x(iy)^2+x(iz)^2);
        end
    end
end
Phi  = exp(-ir2*rr);
if ipot == 1
    rr   = mu*sqrt(rr);
    Epot = (sech(rr)).^2;
elseif ipot == 2
    rr   = mu2*rr;
    Epot = exp(-rr/4)-4*exp(-rr);
elseif ipot == 3
    rr   = mu*sqrt(rr);
    Epot = exp(-rr)-2*exp(-2*rr);
end
% r13
rr = zeros(Nx,Nx,Nx,Ny,Ny,Ny);
for ix = 1:Ny
    for iy = 1:Ny           
          rr(ix,:,:,iy,:,:) = rr(ix,:,:,iy,:,:) + ( x(ix)/2+y(iy))^2;
          rr(:,ix,:,:,iy,:) = rr(:,ix,:,:,iy,:) + ( x(ix)/2+y(iy))^2;
          rr(:,:,ix,:,:,iy) = rr(:,:,ix,:,:,iy) + ( x(ix)/2+y(iy))^2;
    end
end
Phi  = Phi.*exp(-ir2*rr);
if ipot == 1
    rr   = mu*sqrt(rr);
    Epot = Epot + (sech(rr)).^2;
elseif ipot == 2
    rr   = mu2*rr;
    Epot = Epot + exp(-rr/4)-4*exp(-rr);
elseif ipot == 3
    rr   = mu*sqrt(rr);
    Epot = Epot + exp(-rr)-2*exp(-2*rr);
end
% r23
rr = zeros(Nx,Nx,Nx,Ny,Ny,Ny);
for ix = 1:Nx
    for iy = 1:Nx          
          rr(ix,:,:,iy,:,:) = rr(ix,:,:,iy,:,:) + (-x(ix)/2+y(iy))^2;
          rr(:,ix,:,:,iy,:) = rr(:,ix,:,:,iy,:) + (-x(ix)/2+y(iy))^2;
          rr(:,:,ix,:,:,iy) = rr(:,:,ix,:,:,iy) + (-x(ix)/2+y(iy))^2;
    end
end
Phi  = Phi.*exp(-ir2*rr);
if ipot == 1
    rr   = mu*sqrt(rr);
    Epot = Epot + (sech(rr)).^2;
elseif ipot == 2
    rr   = mu2*rr;
    Epot = Epot + exp(-rr/4)-4*exp(-rr);
elseif ipot == 3
    rr   = mu*sqrt(rr);
    Epot = Epot + exp(-rr)-2*exp(-2*rr);
end
Epot = U0*Epot;
snorm = sqrt(sum(sum(sum( sum(sum(sum( Phi.^2 ))) )))*dV);
Phi   = Phi/snorm;
%---------------------------------------------------------------------------------------------
% Kinetic energy
% d^2/dr1^2+d^2/dr2^2+d^2/dr3^3+d^2/dr4^4 = 2 d^2/dX^2 + 2 d^2/dY^2 + d^2/dZ^2 + 1/4 d^2/dR^2
%---------------------------------------------------------------------------------------------
% kx  = 2*pi/Lx*[0:Nx/2-1,-Nx/2:-1];  % this works only for even values 
% ky  = 2*pi/Ly*[0:Nx/2-1,-Nx/2:-1];  % of Nx, Ny, and Nz
% kz  = 2*pi/Lz*[0:Nx/2-1,-Nx/2:-1];  %
kx = 2*pi/Lx*[0:ceil(Nx/2)-1,ceil(-Nx/2):-1];
ky = 2*pi/Ly*[0:ceil(Ny/2)-1,ceil(-Ny/2):-1];
kx2 = kx.^2*2;
ky2 = ky.^2*3/2;
Ekin = zeros(Nx,Nx,Nx,Ny,Ny,Ny);
for n = 1:Nx
Ekin(n,:,:,:,:,:) = Ekin(n,:,:,:,:,:) + kx2(n);
Ekin(:,n,:,:,:,:) = Ekin(:,n,:,:,:,:) + kx2(n);
Ekin(:,:,n,:,:,:) = Ekin(:,:,n,:,:,:) + kx2(n);
end
for n = 1:Ny
Ekin(:,:,:,n,:,:) = Ekin(:,:,:,n,:,:) + ky2(n);
Ekin(:,:,:,:,n,:) = Ekin(:,:,:,:,n,:) + ky2(n);
Ekin(:,:,:,:,:,n) = Ekin(:,:,:,:,:,n) + ky2(n);
end
Ekin = Ekin * hbarc^2/(2*m_nc2);
toc
%===================================================================================
k_lan    = 75;     % maximal size of Lanczos matrix
lan_step = 5;      % initial size and step on Lanczos's iterations
ii = 0;                  % compare different Lanczos matrix sizes         
Il     = zeros(floor(k_lan/lan_step),1);
energy = zeros(floor(k_lan/lan_step),5);
% ----------------------------------------------------------------------------------
fftw('planner','measure');
 
HPhi    = real(ifftn(Ekin.*fftn(Phi)));
Ekin_in = sum(sum(sum( sum(sum(sum( Phi.*HPhi  ))) )))*dV;
HPhi    = HPhi + Epot.*Phi;
alpha   = sum(sum(sum( sum(sum(sum( Phi.*HPhi  ))) )))*dV;

disp([Ekin_in, alpha])

HPhi    = HPhi - alpha*Phi;
A = zeros(k_lan,1);
B = A;
Lanczos = zeros(k_lan,k_lan);
Lanczos(1,1) = alpha;
A(1) = alpha;
B(1) = 0;
for ik = 2:k_lan
    beta  = sqrt( sum(sum(sum( sum(sum(sum( HPhi.*HPhi ))) )))*dV );
    Phi0  = Phi;
    Phi   = HPhi/beta;
    HPhi  = real(ifftn(Ekin.*fftn(Phi))) + Epot.*Phi - beta*Phi0;
    alpha =       sum(sum(sum( sum(sum(sum( Phi.*HPhi ))) )))*dV;
    HPhi  = HPhi - alpha*Phi;
    Lanczos(ik  ,ik-1) = beta;
    Lanczos(ik-1,ik  ) = beta;
    Lanczos(ik  ,ik  ) = alpha;
    A(ik) = alpha;
    B(ik) = beta;
    if mod(ik,lan_step) == 0
        Energy = sort( eig( Lanczos(1:ik,1:ik) ) );
        ii = ii + 1;
        Il(ii) = ik;
        energy(ii,1:5) = Energy(1:5)';
        disp([toc,ii,Il(ii),energy(ii,:)])
    end
end
%===================================================================================
figure(3);
set(gca,'FontSize',20)
plot(Il,energy(:,1),'bo-',Il,energy(:,2),'bo:',Il,0*energy(:,1),'k', ...
     Il,energy(:,3),'go-',Il,energy(:,4),'go:',Il,energy(:,5),'go--')
xlabel('Lanczos''s iteration')
ylabel('Energy [MeV]')
% ylim([-50 100])
hold on
figure(4)
set(gca,'FontSize',20)
plot((1:k_lan),A,'bo-',(2:k_lan),B(2:end),'go-')
xlabel('row number')
ylabel('\alpha and \beta')
title('matrix elements \alpha and \beta')
hold on
%===================================================================================
il(idx,iNx) = 1/Lx;
en(idx,iNx) = energy(end,1);
end
end
hold off

U0, ipot, il, en