%---------------------------------------------------------------------------------------------
% n-dimensional kinetic and potential energy
% minimal use of n-dimensional arrays and vectorization
%---------------------------------------------------------------------------------------------
clear all
format short g
format compact
%---------------------------------------------------------------------------------------------
savefile1 = 'Phi10.mat';
savefile2 = 'Phi11.mat';
iwrite = 0;  % write initial data
% iwrite = 1;  % read initial data
% iwrite = 2;  % read improved data
%---------------------------------------------------------------------------------------------
Lx  = 12;
Ly  = Lx;
Lz  = Ly;
Nx  = 4;
izz = 1;  % if izz == 2 dz = dx/izz and Nz = Nx*izz, otherwise izz = 1
Ny  = Nx;
Nz  = izz*Nx;
dx  = Lx/Nx;
dy  = Ly/Ny;
dz  = Lz/Nz;
x   = dx*(-Nx/2:Nx/2-1);
y   = dy*(-Ny/2:Ny/2-1);
z   = dz*(-Nz/2:Nz/2-1);
dV  = (dx*dy*dz)^3;
%---------------------------------------------------------------------------------------------
hbarc   = 197.326;
m_nc2   = 939.565;
m_pc2   = 938.272;

% ipot   = 1;   % inverse cosh potential
ipot   = 2;   % two gaussians
% ipot   = 3;   % two exponetials
r0     = 1.5; % radius of the potential
mu     = 1/r0;
mu2    = mu^2;
if ipot == 1
    U0     =     -4*mu2*hbarc^2/(2*m_nc2)*1.5;
elseif ipot == 2
    U0     = -3.144*mu2*hbarc^2/(2*m_nc2)*1.75;
elseif ipot == 3
    U0     = -4.764*mu2*hbarc^2/(2*m_nc2);
end
%---------------------------------------------------------------------------------------------
% potential parameters taken from 
% Forbes, Gandolfi, and Gezerlis, Phys.Rev. A 86, 053603 (2012)
%---------------------------------------------------------------------------------------------
ir2    = mu2/2; % radius of initial wave function
Ec     = hbarc^2*pi^2/(2*m_nc2*dx^2);
Lambda = hbarc*pi/dx;
if iwrite <=1
%     tau    = 3*hbarc/Ec;  % step in imaginary time
%     nsteps = 10;
    tau    = 0.25*hbarc/Ec;
    nsteps = 100;
    esteps = 10;
elseif iwrite == 2
    tau    = 0.25*hbarc/Ec;
    nsteps = 30;
    esteps = 10;
end
disp([Nx, Ny, Nz,(Nx*Ny*Nz)^3]) 
disp([Lx, Ly, Lz, dx, dy, dz])
disp([ipot, Ec, Lambda])
tic
%------------------------------------------
% coordinates
%------------------------------------------
% X = r1 - r2
% Y = r3 - r4
% Z = (r1 + r2 - r3 - r4)/2
% R = (r1 + r2 + r3 + r4)/4
%------------------------------------------
% r1 = ( X + Z + 2R)/2
% r2 = (-X + Z + 2R)/2
% r3 = ( Y - Z + 2R)/2
% r4 = (-Y - Z + 2R)/2
%------------------------------------------
% r1 - r2 = X
% r3 - r4 = Y
% r1 - r3 = (  X - Y + 2Z)/2
% r1 - r4 = (  X + Y + 2Z)/2
% r2 - r3 = (- X - Y + 2Z)/2
% r2 - r4 = (- X + Y + 2Z)/2
%------------------------------------------
% sum_{i<j}r_{ij}^2/4 = (X^2+Y^2+2Z^2)/2
%----------------------------------------------------------------
% units fm and MeV
% Potential energy, intial wave function, and sum_{i<j}r_{ij}^2/4
%----------------------------------------------------------------
if iwrite == 0
rr   = zeros(Nx,Nx,Nx,Ny,Ny,Ny,Nz,Nz,Nz);
% r12
for ix = 1:Nx
    for iy = 1:Nx
        for iz = 1:Nx
          rr(ix,iy,iz,:,:,:,:,:,:) = rr(ix,iy,iz,:,:,:,:,:,:) + (x(ix)^2+x(iy)^2+x(iz)^2);
        end
    end
end
Phi  = exp(-ir2*rr);
RR   = rr/2;
if ipot == 1
    rr   = mu*sqrt(rr);
    Epot = (sech(mu*rr)).^2;
elseif ipot == 2
    rr   = mu2*rr;
    Epot = exp(-rr/4)-4*exp(-rr);
elseif ipot == 3
    rr   = mu*sqrt(rr);
    Epot = exp(-rr)-2*exp(-2*rr);
end
% r34
rr   = zeros(Nx,Nx,Nx,Ny,Ny,Ny,Nz,Nz,Nz);
for ix = 1:Ny
    for iy = 1:Ny
        for iz = 1:Ny
          rr(:,:,:,ix,iy,iz,:,:,:) = rr(:,:,:,ix,iy,iz,:,:,:) + (y(ix)^2+y(iy)^2+y(iz)^2);
        end
    end
end
Phi  = Phi.*exp(-ir2*rr);
RR   = RR + rr/2;
if ipot == 1
    rr   = mu*sqrt(rr);
    Epot = Epot + (sech(rr)).^2;
elseif ipot == 2
    rr   = mu2*rr;
    Epot = Epot + exp(-rr/4)-4*exp(-rr);
elseif ipot == 3
    rr   = mu*sqrt(rr);
    Epot = Epot + exp(-rr)-2*exp(-2*rr);
end
% r13
rr = zeros(Nx,Nx,Nx,Ny,Ny,Ny,Nz,Nz,Nz);
for ix = 1:Nx
    for iy = 1:Ny
        for iz = 1:Nz            
          rr(ix,:,:,iy,:,:,iz,:,:) = rr(ix,:,:,iy,:,:,iz,:,:) + ( x(ix)-y(iy)+2*z(iz))^2;
          rr(:,ix,:,:,iy,:,:,iz,:) = rr(:,ix,:,:,iy,:,:,iz,:) + ( x(ix)-y(iy)+2*z(iz))^2;
          rr(:,:,ix,:,:,iy,:,:,iz) = rr(:,:,ix,:,:,iy,:,:,iz) + ( x(ix)-y(iy)+2*z(iz))^2;
        end
    end
end
rr   = rr/4;
if ipot == 1
    rr   = mu*sqrt(rr);
    Epot = Epot + (sech(rr)).^2;
elseif ipot == 2
    rr   = mu2*rr;
    Epot = Epot + exp(-rr/4)-4*exp(-rr);
elseif ipot == 3
    rr   = mu*sqrt(rr);
    Epot = Epot + exp(-rr)-2*exp(-2*rr);
end
% r14
rr = zeros(Nx,Nx,Nx,Ny,Ny,Ny,Nz,Nz,Nz);
for ix = 1:Nx
    for iy = 1:Ny
        for iz = 1:Nz            
          rr(ix,:,:,iy,:,:,iz,:,:) = rr(ix,:,:,iy,:,:,iz,:,:) + ( x(ix)+y(iy)+2*z(iz))^2;
          rr(:,ix,:,:,iy,:,:,iz,:) = rr(:,ix,:,:,iy,:,:,iz,:) + ( x(ix)+y(iy)+2*z(iz))^2;
          rr(:,:,ix,:,:,iy,:,:,iz) = rr(:,:,ix,:,:,iy,:,:,iz) + ( x(ix)+y(iy)+2*z(iz))^2;
        end
    end
end
rr   = rr/4;
if ipot == 1
    rr   = mu*sqrt(rr);
    Epot = Epot + (sech(rr)).^2;
elseif ipot == 2
    rr   = mu2*rr;
    Epot = Epot + exp(-rr/4)-4*exp(-rr);
elseif ipot == 3
    rr   = mu*sqrt(rr);
    Epot = Epot + exp(-rr)-2*exp(-2*rr);
end
% r23
rr = zeros(Nx,Nx,Nx,Ny,Ny,Ny,Nz,Nz,Nz);
for ix = 1:Nx
    for iy = 1:Ny
        for iz = 1:Nz            
          rr(ix,:,:,iy,:,:,iz,:,:) = rr(ix,:,:,iy,:,:,iz,:,:) + (-x(ix)-y(iy)+2*z(iz))^2;
          rr(:,ix,:,:,iy,:,:,iz,:) = rr(:,ix,:,:,iy,:,:,iz,:) + (-x(ix)-y(iy)+2*z(iz))^2;
          rr(:,:,ix,:,:,iy,:,:,iz) = rr(:,:,ix,:,:,iy,:,:,iz) + (-x(ix)-y(iy)+2*z(iz))^2;
        end
    end
end
rr   = rr/4;
if ipot == 1
    rr   = mu*sqrt(rr);
    Epot = Epot + (sech(rr)).^2;
elseif ipot == 2
    rr   = mu2*rr;
    Epot = Epot + exp(-rr/4)-4*exp(-rr);
elseif ipot == 3
    rr   = mu*sqrt(rr);
    Epot = Epot + exp(-rr)-2*exp(-2*rr);
end
% r24
rr = zeros(Nx,Nx,Nx,Ny,Ny,Ny,Nz,Nz,Nz);
for ix = 1:Nx
    for iy = 1:Ny
        for iz = 1:Nz            
          rr(ix,:,:,iy,:,:,iz,:,:) = rr(ix,:,:,iy,:,:,iz,:,:) + (-x(ix)+y(iy)+2*z(iz))^2;
          rr(:,ix,:,:,iy,:,:,iz,:) = rr(:,ix,:,:,iy,:,:,iz,:) + (-x(ix)+y(iy)+2*z(iz))^2;
          rr(:,:,ix,:,:,iy,:,:,iz) = rr(:,:,ix,:,:,iy,:,:,iz) + (-x(ix)+y(iy)+2*z(iz))^2;
        end
    end
end
rr   = rr/4;
if ipot == 1
    rr   = mu*sqrt(rr);
    Epot = Epot + (sech(rr)).^2;
elseif ipot == 2
    rr   = mu2*rr;
    Epot = Epot + exp(-rr/4)-4*exp(-rr);
elseif ipot == 3
    rr   = mu*sqrt(rr);
    Epot = Epot + exp(-rr)-2*exp(-2*rr);
end
Epot = U0*Epot;
% last contribution to the intial wave function - z^2
% phi(initial) = exp( -ir2*sum_{i<j}r_{ij}^2/2 )= exp( -ir2(X^2+Y^2+2Z^2) )
rr = zeros(Nx,Nx,Nx,Ny,Ny,Ny,Nz,Nz,Nz);
for ix = 1:Nz
    for iy = 1:Nz
        for iz = 1:Nz            
          rr(:,:,:,:,:,:,ix,iy,iz) = rr(:,:,:,:,:,:,ix,iy,iz) + 2*(z(ix)^2+z(iy)^2+z(iz)^2);
        end
    end
end
Phi = Phi.*exp(-ir2*rr);
snorm = sqrt(sum(sum(sum(sum(sum(sum(sum(sum(sum(Phi.^2)))))))))*dV);
Phi   = Phi/snorm;
RR    = RR + rr;
%---------------------------------------------------------------------------------------------
% Kinetic energy
% d^2/dr1^2+d^2/dr2^2+d^2/dr3^3+d^2/dr4^4 = 2 d^2/dX^2 + 2 d^2/dY^2 + d^2/dZ^2 + 1/4 d^2/dR^2
%---------------------------------------------------------------------------------------------
% k1*r1+k2*r2+k3*r3+k4*r4 = 
%     (k1-k2)/2*X + (k3-k4)/2*Y + (k1+k2-k3-k4)/2*Z +(k1+k2+k3+k4)*R 
%---------------------------------------------------------------------------------------------
% kx  = 2*pi/Lx*[0:Nx/2-1,-Nx/2:-1];  % this works only for even values 
% ky  = 2*pi/Ly*[0:Nx/2-1,-Nx/2:-1];  % of Nx, Ny, and Nz
% kz  = 2*pi/Lz*[0:Nx/2-1,-Nx/2:-1];  %
kx = 2*pi/Lx*[0:ceil(Nx/2)-1,ceil(-Nx/2):-1];
ky = 2*pi/Ly*[0:ceil(Ny/2)-1,ceil(-Ny/2):-1];
kz = 2*pi/Lz*[0:ceil(Nz/2)-1,ceil(-Nz/2):-1];
kx2 = kx.^2*2;
ky2 = ky.^2*2;
kz2 = kz.^2;
Ekin = zeros(Nx,Nx,Nx,Ny,Ny,Ny,Nz,Nz,Nz);
for n = 1:Nx
Ekin(n,:,:,:,:,:,:,:,:) = Ekin(n,:,:,:,:,:,:,:,:) + kx2(n);
Ekin(:,n,:,:,:,:,:,:,:) = Ekin(:,n,:,:,:,:,:,:,:) + kx2(n);
Ekin(:,:,n,:,:,:,:,:,:) = Ekin(:,:,n,:,:,:,:,:,:) + kx2(n);
end
for n = 1:Ny
Ekin(:,:,:,n,:,:,:,:,:) = Ekin(:,:,:,n,:,:,:,:,:) + ky2(n);
Ekin(:,:,:,:,n,:,:,:,:) = Ekin(:,:,:,:,n,:,:,:,:) + ky2(n);
Ekin(:,:,:,:,:,n,:,:,:) = Ekin(:,:,:,:,:,n,:,:,:) + ky2(n);
end
for n = 1:Nz
Ekin(:,:,:,:,:,:,n,:,:) = Ekin(:,:,:,:,:,:,n,:,:) + kz2(n);
Ekin(:,:,:,:,:,:,:,n,:) = Ekin(:,:,:,:,:,:,:,n,:) + kz2(n);
Ekin(:,:,:,:,:,:,:,:,n) = Ekin(:,:,:,:,:,:,:,:,n) + kz2(n);
end
Ekin = Ekin * hbarc^2/(2*m_nc2);
%---------------------------------------------------------------------------------------------
    save(savefile1,'Phi','Ekin','Epot','RR')    
elseif iwrite == 1  
    load(savefile1,'Phi','Ekin','Epot','RR')
    disp(iwrite)
elseif iwrite == 2
    load(savefile2,'Phi','Ekin','Epot','RR')
    disp(iwrite)
end    
toc
%---------------------------------------------------------------------------------------------
tauh  = tau/hbarc;
exp_K = exp(-Ekin*tauh);

tauh2  = tauh/2;
exp_V  = exp(-Epot*tauh2);
exp_V2 = exp_V.^2;

%==========================================================================
itime  = zeros(floor(nsteps/esteps),1);
nPhi   = itime;
Energy = itime;
r2     = itime;
fftw('planner','measure');
disp(fftw('planner'))
tic

ie     = 0;
for it = 1:nsteps
    if mod(it,esteps) == 1 || esteps == 1
        Phi        = exp_V.*Phi; 
    end
        Phi        = real(ifftn(exp_K.*fftn(Phi)));
    if  ~( mod(it,esteps) == 0 ||  it == nsteps )
        Phi        = exp_V2.*Phi;
    elseif mod(it,esteps) == 0 || it == nsteps
        ie = ie + 1;
        Phi = exp_V.*Phi;
        snorm      = sqrt(sum(sum(sum(sum(sum(sum(sum(sum(sum(Phi.^2)))))))))*dV);
        Phi        = Phi/snorm;
        Ener       = sum(sum(sum(sum(sum(sum(sum(sum(sum( ...
            Phi.*(real(ifftn(Ekin.*fftn(Phi))) + Epot.*Phi) )))))))))*dV;
        Energy(ie) = real(Ener);
        r2(ie)     = sqrt(sum(sum(sum(sum(sum(sum(sum(sum(sum(Phi.^2.*RR)))))))))*dV/4);
        itime(ie)  = it*tau;
        disp([it, ie, toc, itime(ie), Energy(ie), r2(ie), snorm])
    end
end
save(savefile2,'Phi','Ekin','Epot','RR')

figure(3)
set(gca,'FontSize',20)
plot(itime,Energy,'bs-')
xlabel('tau [fm/c]')
ylabel('Energy [MeV]')
ylim([-30 100])
hold on
figure(5)
set(gca,'FontSize',20)
plot(itime,r2,'bs-')
xlabel('tau [fm/c]')
ylabel('radius [fm]')
hold on
%==========================================================================