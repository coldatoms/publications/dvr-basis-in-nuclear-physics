%---------------------------------------------------------------------------------------------
% n-dimensional kinetic and potential energy
% minimal use of n-dimensional arrays and vectorization
%---------------------------------------------------------------------------------------------
clear all
format short g
format compact
il = zeros(3,5);
en = il;
idx = 0;
for dx = 0.5:0.25:1.5
idx = idx + 1;
iNx = 0;
for Nx = 8:2:16
    iNx = iNx + 1;
%---------------------------------------------------------------------------------------------
Lx  = Nx*dx;
Ly  = Lx;
Lz  = Lx;
%Nx  = 8;  
Ny  = Nx;
%dx  = Lx/Nx;
dy  = Ly/Ny;
x   = dx*(-Nx/2:Nx/2-1);
y   = dy*(-Ny/2:Ny/2-1);
dV  = (dx*dx)^3;
%---------------------------------------------------------------------------------------------
hbarc   = 197.326;
m_nc2   = 939.565;
m_pc2   = 938.272;

% ipot   = 1;   % inverse cosh potential
ipot   = 2;   % two gaussians
% ipot   = 3;   % two exponetials
r0     = 1.5; % radius of the potential
mu     = 1/r0;
mu2    = mu^2;
if ipot == 1
    U0     =     -4*mu2*hbarc^2/(2*m_nc2)*1;
elseif ipot == 2
    U0     = -3.144*mu2*hbarc^2/(2*m_nc2)*4;
elseif ipot == 3
    U0     = -4.764*mu2*hbarc^2/(2*m_nc2);
end
%---------------------------------------------------------------------------------------------
% potentials taken from Forbes, Gandolfi, and Gezerlis, Phys.Rev. A 86, 053603 (2012)
%%---------------------------------------------------------------------------------------------
ir2    = mu2/2; % radius of initial wave function
Lambda = hbarc*pi/dx;
Ec     = Lambda^2/(2*m_nc2);
disp([Nx, Ny,(Nx*Ny)^3]) 
disp([Lx, Ly, dx, dy])
disp([ipot, U0, mu, Ec, Lambda])
tic
% coordinates
% --------------------------
% X =  r1 - r2
% Y = (r1 + r2 - 2 r3)/2
% R = (r1 + r2 +   r3 )/3
% --------------------------
% r1 =   X/2 +   Y/3 + R
% r2 =  -X/2 +   Y/3 + R
% r3 =       - 2 Y/3 + R
% --------------------------
% r1 - r2 =  X
% r1 - r3 =  X/2 + Y 
% r2 - r3 = -X/2 + Y 
% --------------------------
%------------------------------------------------------------------------
% k1*r1 + k2*r2 + k3*r3  = 
%     (k1 - k2)/2*X + (k1 + k2 - 2 k3)/3*Y + (k1 + k2 + k3)*R 
%------------------------------------------------------------------------
% units fm and MeV
% Potential energy and intial wave function
%---------------------------------------------------------------------------------------------
rr   = zeros(Nx,Nx,Nx,Ny,Ny,Ny);
% r12
for ix = 1:Nx
    for iy = 1:Nx
        for iz = 1:Nx
          rr(ix,iy,iz,:,:,:) = rr(ix,iy,iz,:,:,:) + (x(ix)^2+x(iy)^2+x(iz)^2);
        end
    end
end
Phi  = exp(-ir2*rr);
RR   = rr;
if ipot == 1
    rr   = mu*sqrt(rr);
    Epot = (sech(rr)).^2;
elseif ipot == 2
    rr   = mu2*rr;
    Epot = exp(-rr/4)-4*exp(-rr);
elseif ipot == 3
    rr   = mu*sqrt(rr);
    Epot = exp(-rr)-2*exp(-2*rr);
end
% r13
rr = zeros(Nx,Nx,Nx,Ny,Ny,Ny);
for ix = 1:Ny
    for iy = 1:Ny           
          rr(ix,:,:,iy,:,:) = rr(ix,:,:,iy,:,:) + ( x(ix)/2+y(iy))^2;
          rr(:,ix,:,:,iy,:) = rr(:,ix,:,:,iy,:) + ( x(ix)/2+y(iy))^2;
          rr(:,:,ix,:,:,iy) = rr(:,:,ix,:,:,iy) + ( x(ix)/2+y(iy))^2;
    end
end
Phi  = Phi.*exp(-ir2*rr);
RR   = RR + rr;
if ipot == 1
    rr   = mu*sqrt(rr);
    Epot = Epot + (sech(rr)).^2;
elseif ipot == 2
    rr   = mu2*rr;
    Epot = Epot + exp(-rr/4)-4*exp(-rr);
elseif ipot == 3
    rr   = mu*sqrt(rr);
    Epot = Epot + exp(-rr)-2*exp(-2*rr);
end
% r23
rr = zeros(Nx,Nx,Nx,Ny,Ny,Ny);
for ix = 1:Nx
    for iy = 1:Nx          
          rr(ix,:,:,iy,:,:) = rr(ix,:,:,iy,:,:) + (-x(ix)/2+y(iy))^2;
          rr(:,ix,:,:,iy,:) = rr(:,ix,:,:,iy,:) + (-x(ix)/2+y(iy))^2;
          rr(:,:,ix,:,:,iy) = rr(:,:,ix,:,:,iy) + (-x(ix)/2+y(iy))^2;
    end
end
Phi  = Phi.*exp(-ir2*rr);
RR   = RR + rr;
if ipot == 1
    rr   = mu*sqrt(rr);
    Epot = Epot + (sech(rr)).^2;
elseif ipot == 2
    rr   = mu2*rr;
    Epot = Epot + exp(-rr/4)-4*exp(-rr);
elseif ipot == 3
    rr   = mu*sqrt(rr);
    Epot = Epot + exp(-rr)-2*exp(-2*rr);
end
Epot = U0*Epot;
snorm = sqrt(sum(sum(sum( sum(sum(sum( Phi.^2 ))) )))*dV);
Phi   = Phi/snorm;
%---------------------------------------------------------------------------------------------
% Kinetic energy
% d^2/dr1^2+d^2/dr2^2+d^2/dr3^3+d^2/dr4^4 = 2 d^2/dX^2 + 2 d^2/dY^2 + d^2/dZ^2 + 1/4 d^2/dR^2
%---------------------------------------------------------------------------------------------
% kx  = 2*pi/Lx*[0:Nx/2-1,-Nx/2:-1];  % this works only for even values 
% ky  = 2*pi/Ly*[0:Nx/2-1,-Nx/2:-1];  % of Nx, Ny, and Nz
% kz  = 2*pi/Lz*[0:Nx/2-1,-Nx/2:-1];  %
kx = 2*pi/Lx*[0:ceil(Nx/2)-1,ceil(-Nx/2):-1];
ky = 2*pi/Ly*[0:ceil(Ny/2)-1,ceil(-Ny/2):-1];
kx2 = kx.^2*2;
ky2 = ky.^2*3/2;
Ekin = zeros(Nx,Nx,Nx,Ny,Ny,Ny);
for n = 1:Nx
Ekin(n,:,:,:,:,:) = Ekin(n,:,:,:,:,:) + kx2(n);
Ekin(:,n,:,:,:,:) = Ekin(:,n,:,:,:,:) + kx2(n);
Ekin(:,:,n,:,:,:) = Ekin(:,:,n,:,:,:) + kx2(n);
end
for n = 1:Ny
Ekin(:,:,:,n,:,:) = Ekin(:,:,:,n,:,:) + ky2(n);
Ekin(:,:,:,:,n,:) = Ekin(:,:,:,:,n,:) + ky2(n);
Ekin(:,:,:,:,:,n) = Ekin(:,:,:,:,:,n) + ky2(n);
end
Ekin = Ekin * hbarc^2/(2*m_nc2);
toc
%===================================================================================
tau    = 0.25*hbarc/Ec;
nsteps = 150;
esteps = 10;
    
tauh  = tau/hbarc;
exp_K = exp(-Ekin*tauh);

tauh2  = tauh/2;
exp_V  = exp(-Epot*tauh2);
exp_V2 = exp_V.^2;
% ----------------------------------------------------------------------------------
fftw('planner','measure');
 
itime  = zeros(floor(nsteps/esteps),1);
nPhi   = itime;
Energy = itime;
r2     = itime;
fftw('planner','measure');
disp(fftw('planner'))
tic

ie     = 0;
for it = 1:nsteps
    if mod(it,esteps) == 1 || esteps == 1
        Phi        = exp_V.*Phi; 
    end
        Phi        = real(ifftn(exp_K.*fftn(Phi)));
    if  ~( mod(it,esteps) == 0 ||  it == nsteps )
        Phi        = exp_V2.*Phi;
    elseif mod(it,esteps) == 0 || it == nsteps
        ie = ie + 1;
        Phi = exp_V.*Phi;
        snorm      = sqrt(sum(sum(sum( sum(sum(sum( Phi.^2 ))) )))*dV);
        Phi        = Phi/snorm;
        nPhi(ie)   = snorm;
        Ener       = sum(sum(sum( sum(sum(sum(  ...
            Phi.*(real(ifftn(Ekin.*fftn(Phi))) + Epot.*Phi)  ))) )))*dV;
        Energy(ie) = real(Ener);
        r2(ie)     = sqrt(sum(sum(sum( sum(sum(sum( Phi.^2.*RR ))) )))*dV/4);
        itime(ie)  = it*tau;
        disp([it, ie, toc, itime(ie), Energy(ie), r2(ie), snorm])
    end
end

%===================================================================================
figure(5)
set(gca,'FontSize',20)
plot(itime,Energy,'bs-')
xlabel('tau [fm/c]')
ylabel('Energy [MeV]')
hold on
figure(6)
set(gca,'FontSize',20)
plot(itime,r2,'bs-')
xlabel('tau [fm/c]')
ylabel('radius [fm]')
hold on
%===================================================================================
end
end
hold off