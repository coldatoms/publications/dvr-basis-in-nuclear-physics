%---------------------------------------------------------------------------------------------
% n-dimensional kinetic and potential energy
% minimal use of n-dimensional arrays and vectorization
%---------------------------------------------------------------------------------------------
clear all
format short g
format compact
%---------------------------------------------------------------------------------------------
savefile1 = 'Phi_L_ini.mat';
iwrite = 0;  % write initial data
% iwrite = 1;  % read initial data
%---------------------------------------------------------------------------------------------
Lx  = 8;
Ly  = Lx;
Lz  = Lx;
Nx  = 4;  
izz = 1;  % if izz == 2 dz = dx/izz and Nz = Nx*izz, otherwise izz = 1
Ny  = Nx;
Nz  = izz*Nx;
dx  = Lx/Nx;
dy  = Ly/Ny;
dz  = Lz/Nz;
x   = dx*(-Nx/2:Nx/2-1);
y   = dy*(-Ny/2:Ny/2-1);
z   = dz*(-Nz/2:Nz/2-1);
dV  = dx^9;
%---------------------------------------------------------------------------------------------
hbarc   = 197.326;
m_nc2   = 939.565;
m_pc2   = 938.272;

% ipot   = 1;   % inverse cosh potential
ipot   = 2;   % two gaussians
% ipot   = 3;   % two exponetials
r0     = 1.5; % radius of the potential
mu     = 1/r0;
mu2    = mu^2;
if ipot == 1
    U0     =     -4*mu2*hbarc^2/(2*m_nc2)*1.5;
elseif ipot == 2
    U0     = -3.144*mu2*hbarc^2/(2*m_nc2)*2.5;
elseif ipot == 3
    U0     = -4.764*mu2*hbarc^2/(2*m_nc2);
end
%---------------------------------------------------------------------------------------------
% potential parameters taken from 
% Forbes, Gandolfi, and Gezerlis, Phys.Rev. A 86, 053603 (2012)
%%---------------------------------------------------------------------------------------------
ir2    = mu2/2; % radius of initial wave function
Lambda = hbarc*pi/dx;
Ec     = Lambda^2/(2*m_nc2);
disp([Nx, Ny, Nz,(Nx*Ny*Nz)^3]) 
disp([Lx, Ly, Lz, dx, dy, dz])
disp([ipot, U0, Ec, Lambda])
tic
% coordinates
% --------------------------
% X = r1 - r2
% Y = r3 - r4
% Z = (r1 + r2 - r3 - r4)/2
% R = (r1 + r2 + r3 + r4)/4
% --------------------------
% r1 = ( X + Z + 2R)/2
% r2 = (-X + Z + 2R)/2
% r3 = ( Y - Z + 2R)/2
% r4 = (-Y - Z + 2R)/2
% --------------------------
% r1 - r2 = X
% r3 - r4 = Y
% r1 - r3 = (  X - Y + 2Z)/2
% r1 - r4 = (  X + Y + 2Z)/2
% r2 - r3 = (- X - Y + 2Z)/2
% r2 - r4 = (- X + Y + 2Z)/2
% --------------------------
%------------------------------------------------------------------------
% k1*r1+k2*r2+k3*r3+k4*r4 = 
%     (k1-k2)/2*X + (k3-k4)/2*Y + (k1+k2-k3-k4)/2*Z +(k1+k2+k3+k4)*R 
%------------------------------------------------------------------------
% units fm and MeV
% Potential energy and intial wave function
%---------------------------------------------------------------------------------------------
if iwrite == 0
rr   = zeros(Nx,Nx,Nx,Nx,Nx,Nx,Nx,Nx,Nx);
% r12
for ix = 1:Nx
    for iy = 1:Nx
        for iz = 1:Nx
          rr(ix,iy,iz,:,:,:,:,:,:) = rr(ix,iy,iz,:,:,:,:,:,:) + (x(ix)^2+x(iy)^2+x(iz)^2);
        end
    end
end
Phi  = exp(-ir2*rr);
if ipot == 1
    rr   = mu*sqrt(rr);
    Epot = (sech(rr)).^2;
elseif ipot == 2
    rr   = mu2*rr;
    Epot = exp(-rr/4)-4*exp(-rr);
elseif ipot == 3
    rr   = mu*sqrt(rr);
    Epot = exp(-rr)-2*exp(-2*rr);
end
% r34
rr   = zeros(Nx,Nx,Nx,Nx,Nx,Nx,Nx,Nx,Nx);
for ix = 1:Nx
    for iy = 1:Nx
        for iz = 1:Nx
          rr(:,:,:,ix,iy,iz,:,:,:) = rr(:,:,:,ix,iy,iz,:,:,:) + (y(ix)^2+y(iy)^2+y(iz)^2);
        end
    end
end
Phi  = Phi.*exp(-ir2*rr);
if ipot == 1
    rr   = mu*sqrt(rr);
    Epot = Epot + (sech(rr)).^2;
elseif ipot == 2
    rr   = mu2*rr;
    Epot = Epot + exp(-rr/4)-4*exp(-rr);
elseif ipot == 3
    rr   = mu*sqrt(rr);
    Epot = Epot + exp(-rr)-2*exp(-2*rr);
end
% r13
rr = zeros(Nx,Nx,Nx,Nx,Nx,Nx,Nx,Nx,Nx);
for ix = 1:Nx
    for iy = 1:Nx
        for iz = 1:Nx            
          rr(ix,:,:,iy,:,:,iz,:,:) = rr(ix,:,:,iy,:,:,iz,:,:) + ( x(ix)-y(iy)+2*z(iz))^2;
          rr(:,ix,:,:,iy,:,:,iz,:) = rr(:,ix,:,:,iy,:,:,iz,:) + ( x(ix)-y(iy)+2*z(iz))^2;
          rr(:,:,ix,:,:,iy,:,:,iz) = rr(:,:,ix,:,:,iy,:,:,iz) + ( x(ix)-y(iy)+2*z(iz))^2;
        end
    end
end
rr   = rr/4;
if ipot == 1
    rr   = mu*sqrt(rr);
    Epot = Epot + (sech(rr)).^2;
elseif ipot == 2
    rr   = mu2*rr;
    Epot = Epot + exp(-rr/4)-4*exp(-rr);
elseif ipot == 3
    rr   = mu*sqrt(rr);
    Epot = Epot + exp(-rr)-2*exp(-2*rr);
end
% r14
rr = zeros(Nx,Nx,Nx,Nx,Nx,Nx,Nx,Nx,Nx);
for ix = 1:Nx
    for iy = 1:Nx
        for iz = 1:Nx            
          rr(ix,:,:,iy,:,:,iz,:,:) = rr(ix,:,:,iy,:,:,iz,:,:) + ( x(ix)+y(iy)+2*z(iz))^2;
          rr(:,ix,:,:,iy,:,:,iz,:) = rr(:,ix,:,:,iy,:,:,iz,:) + ( x(ix)+y(iy)+2*z(iz))^2;
          rr(:,:,ix,:,:,iy,:,:,iz) = rr(:,:,ix,:,:,iy,:,:,iz) + ( x(ix)+y(iy)+2*z(iz))^2;
        end
    end
end
rr   = rr/4;
if ipot == 1
    rr   = mu*sqrt(rr);
    Epot = Epot + (sech(rr)).^2;
elseif ipot == 2
    rr   = mu2*rr;
    Epot = Epot + exp(-rr/4)-4*exp(-rr);
elseif ipot == 3
    rr   = mu*sqrt(rr);
    Epot = Epot + exp(-rr)-2*exp(-2*rr);
end
% r23
rr = zeros(Nx,Nx,Nx,Nx,Nx,Nx,Nx,Nx,Nx);
for ix = 1:Nx
    for iy = 1:Nx
        for iz = 1:Nx            
          rr(ix,:,:,iy,:,:,iz,:,:) = rr(ix,:,:,iy,:,:,iz,:,:) + (-x(ix)-y(iy)+2*z(iz))^2;
          rr(:,ix,:,:,iy,:,:,iz,:) = rr(:,ix,:,:,iy,:,:,iz,:) + (-x(ix)-y(iy)+2*z(iz))^2;
          rr(:,:,ix,:,:,iy,:,:,iz) = rr(:,:,ix,:,:,iy,:,:,iz) + (-x(ix)-y(iy)+2*z(iz))^2;
        end
    end
end
rr   = rr/4;
if ipot == 1
    rr   = mu*sqrt(rr);
    Epot = Epot + (sech(rr)).^2;
elseif ipot == 2
    rr   = mu2*rr;
    Epot = Epot + exp(-rr/4)-4*exp(-rr);
elseif ipot == 3
    rr   = mu*sqrt(rr);
    Epot = Epot + exp(-rr)-2*exp(-2*rr);
end
% r24
rr = zeros(Nx,Nx,Nx,Nx,Nx,Nx,Nx,Nx,Nx);
for ix = 1:Nx
    for iy = 1:Nx
        for iz = 1:Nx            
          rr(ix,:,:,iy,:,:,iz,:,:) = rr(ix,:,:,iy,:,:,iz,:,:) + (-x(ix)+y(iy)+2*z(iz))^2;
          rr(:,ix,:,:,iy,:,:,iz,:) = rr(:,ix,:,:,iy,:,:,iz,:) + (-x(ix)+y(iy)+2*z(iz))^2;
          rr(:,:,ix,:,:,iy,:,:,iz) = rr(:,:,ix,:,:,iy,:,:,iz) + (-x(ix)+y(iy)+2*z(iz))^2;
        end
    end
end
rr   = rr/4;
if ipot == 1
    rr   = mu*sqrt(rr);
    Epot = Epot + (sech(rr)).^2;
elseif ipot == 2
    rr   = mu2*rr;
    Epot = Epot + exp(-rr/4)-4*exp(-rr);
elseif ipot == 3
    rr   = mu*sqrt(rr);
    Epot = Epot + exp(-rr)-2*exp(-2*rr);
end
Epot = U0*Epot;
%---------------------------------------------------------------------------------------------
% last contribution to the intial wave function - z^2
% phi(initial) = exp( -ir2*sum_{i<j}r_{ij}^2/2 )= exp( -ir2(X^2+Y^2+2*Z^2) )
%---------------------------------------------------------------------------------------------
rr = zeros(Nx,Nx,Nx,Nx,Nx,Nx,Nx,Nx,Nx);
for ix = 1:Nx
    for iy = 1:Nx
        for iz = 1:Nx            
          rr(:,:,:,:,:,:,ix,iy,iz) = rr(:,:,:,:,:,:,ix,iy,iz) + 2*(x(ix)^2+y(iy)^2+z(iz)^2);
        end
    end
end
Phi = Phi.*exp(-ir2*rr);
snorm = sqrt(sum(sum(sum(sum(sum(sum(sum(sum(sum(Phi.^2)))))))))*dV);
Phi   = Phi/snorm;
%---------------------------------------------------------------------------------------------
% Kinetic energy
% d^2/dr1^2+d^2/dr2^2+d^2/dr3^3+d^2/dr4^4 = 2 d^2/dX^2 + 2 d^2/dY^2 + d^2/dZ^2 + 1/4 d^2/dR^2
%---------------------------------------------------------------------------------------------
% kx  = 2*pi/Lx*[0:Nx/2-1,-Nx/2:-1];  % this works only for even values 
% ky  = 2*pi/Ly*[0:Nx/2-1,-Nx/2:-1];  % of Nx, Ny, and Nz
% kz  = 2*pi/Lz*[0:Nx/2-1,-Nx/2:-1];  %
kx = 2*pi/Lx*[0:ceil(Nx/2)-1,ceil(-Nx/2):-1];
ky = 2*pi/Ly*[0:ceil(Ny/2)-1,ceil(-Ny/2):-1];
kz = 2*pi/Lz*[0:ceil(Nz/2)-1,ceil(-Nz/2):-1];
kx2 = kx.^2;
ky2 = ky.^2;
kz2 = kz.^2;
Ekin = zeros(Nx,Nx,Nx,Nx,Nx,Nx,Nx,Nx,Nx);
for n = 1:Nx
Ekin(n,:,:,:,:,:,:,:,:) = Ekin(n,:,:,:,:,:,:,:,:) + kx2(n)*2;
Ekin(:,n,:,:,:,:,:,:,:) = Ekin(:,n,:,:,:,:,:,:,:) + kx2(n)*2;
Ekin(:,:,n,:,:,:,:,:,:) = Ekin(:,:,n,:,:,:,:,:,:) + kx2(n)*2;
Ekin(:,:,:,n,:,:,:,:,:) = Ekin(:,:,:,n,:,:,:,:,:) + ky2(n)*2;
Ekin(:,:,:,:,n,:,:,:,:) = Ekin(:,:,:,:,n,:,:,:,:) + ky2(n)*2;
Ekin(:,:,:,:,:,n,:,:,:) = Ekin(:,:,:,:,:,n,:,:,:) + ky2(n)*2;
Ekin(:,:,:,:,:,:,n,:,:) = Ekin(:,:,:,:,:,:,n,:,:) + kz2(n);
Ekin(:,:,:,:,:,:,:,n,:) = Ekin(:,:,:,:,:,:,:,n,:) + kz2(n);
Ekin(:,:,:,:,:,:,:,:,n) = Ekin(:,:,:,:,:,:,:,:,n) + kz2(n);
end
Ekin = Ekin * hbarc^2/(2*m_nc2);
%------------------------------------------------------------------------
    save(savefile1,'Phi','Ekin','Epot')    
elseif iwrite == 1  
    load(savefile1,'Phi','Ekin','Epot')
end    
toc
%===================================================================================
k_lan    = 75;     % maximal size of Lanczos matrix
lan_step = 5;      % initial size and step on Lanczos's iterations
ii = 0;                  % compare different Lanczos matrix sizes         
Il     = zeros(floor(k_lan/lan_step),1);
energy = zeros(floor(k_lan/lan_step),5);
% ----------------------------------------------------------------------------------
fftw('planner','measure');
 
HPhi    = real(ifftn(Ekin.*fftn(Phi)));
Ekin_in = sum(sum(sum( sum(sum(sum( sum(sum(sum( Phi.*HPhi ))) ))) )))*dV;
HPhi    = HPhi + Epot.*Phi;
alpha   = sum(sum(sum( sum(sum(sum( sum(sum(sum( Phi.*HPhi ))) ))) )))*dV;

disp([Ekin_in, alpha])

HPhi    = HPhi - alpha*Phi;
A = zeros(k_lan,1);
B = A;
Lanczos = zeros(k_lan,k_lan);
Lanczos(1,1) = alpha;
A(1) = alpha;
B(1) = 0;
for ik = 2:k_lan
    beta  = sqrt( sum(sum(sum( sum(sum(sum( sum(sum(sum( HPhi.*HPhi ))) ))) )))*dV );
    Phi0  = Phi;
    Phi   = HPhi/beta;
    HPhi  = real(ifftn(Ekin.*fftn(Phi))) + Epot.*Phi - beta*Phi0;
    alpha = sum(sum(sum( sum(sum(sum( sum(sum(sum( Phi.*HPhi ))) ))) )))*dV;
    HPhi  = HPhi - alpha*Phi;
    Lanczos(ik  ,ik-1) = beta;
    Lanczos(ik-1,ik  ) = beta;
    Lanczos(ik  ,ik  ) = alpha;
    A(ik) = alpha;
    B(ik) = beta;
    if mod(ik,lan_step) == 0
        Energy = sort( eig( Lanczos(1:ik,1:ik) ) );
        ii = ii + 1;
        Il(ii) = ik;
        energy(ii,1:5) = Energy(1:5)';
        disp([toc,ii,Il(ii),energy(ii,:)])
    end
end
%===================================================================================
figure(3);
set(gca,'FontSize',20)
plot(Il,energy(:,1),'bo-',Il,energy(:,2),'bo:',Il,0*energy(:,1),'k', ...
     Il,energy(:,3),'go-',Il,energy(:,4),'go:',Il,energy(:,5),'go--')
xlabel('Lanczos''s iteration')
ylabel('Energy [MeV]')
ylim([-30 100])
hold on
figure(4)
set(gca,'FontSize',20)
plot((1:k_lan),A,'bo-',(2:k_lan),B(2:end),'go-')
xlabel('row number')
ylabel('\alpha and \beta')
title('matrix elements \alpha and \beta')
hold on
%===================================================================================